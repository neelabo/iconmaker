﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace IconMaker
{
    public class IconSource : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([global::System.Runtime.CompilerServices.CallerMemberNameAttribute] string name = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
            }
        }
        #endregion

        public int Level { get; set; }

        public int Pixel { get; set; }
        public int DispPixel { get { return Pixel < 64 ? Pixel : 64; } }

        public string Label
        {
            get
            {
                if (Level == 0) return "Default";
                else return string.Format("{0}x{0}", Pixel);
            }
        }

        #region Property: Image
        private BitmapSource _Image;
        public BitmapSource Image
        {
            get { return _Image; }
            set { _Image = value; OnPropertyChanged(); }
        }
        #endregion

        public BitmapScalingMode ViewScalingMode
        {
            get { return (Pixel < 64) ? BitmapScalingMode.NearestNeighbor : BitmapScalingMode.HighQuality; }
        }
    }


}
