﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IconMaker
{
    /// <summary>
    /// FormattedTextBox.xaml の相互作用ロジック
    /// </summary>
    public partial class FormattedTextBox : UserControl
    {
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
            "Text",
            typeof(string),
            typeof(FormattedTextBox),
            new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnTextChanged)));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        private static void OnTextChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        public static readonly DependencyProperty StringFormatProperty =
            DependencyProperty.Register(
            "StringFormat",
            typeof(string),
            typeof(FormattedTextBox),
            new FrameworkPropertyMetadata("{0}", new PropertyChangedCallback(OnStringFormatChanged)));

        public string StringFormat
        {
            get { return (string)GetValue(StringFormatProperty); }
            set { SetValue(StringFormatProperty, value); }
        }

        private static void OnStringFormatChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            FormattedTextBox ctrl = obj as FormattedTextBox;
            if (ctrl != null)
            {
                var binding = new Binding("Text");
                binding.Source = ctrl;
                binding.StringFormat = (string)e.NewValue;
                ctrl.filterTextBlock.SetBinding(TextBlock.TextProperty, binding);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public FormattedTextBox()
        {
            InitializeComponent();

            DataContext = this;
        }
    }
}
