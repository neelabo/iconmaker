﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace IconMaker
{
    public class BitmapMask
    {
        public byte[] Mask { get; private set; }

        //
        public BitmapMask()
        {
        }

        //
        public BitmapMask(BitmapSource bmp)
        {
            Create(bmp);
        }

        /// <summary>
        /// マスク画像作成
        /// </summary>
        /// <param name="bmp">入力画像Bitmap</param>
        //public static byte[] CreateMask(BitmapSource bmp)
        public byte[] Create(BitmapSource bmp)
        {
            if (bmp.Format != PixelFormats.Bgra32 && bmp.Format != PixelFormats.Pbgra32)
            {
                bmp = new FormatConvertedBitmap(bmp, PixelFormats.Bgra32, null, 0);
            }

            var pixels = new int[bmp.PixelWidth * bmp.PixelHeight];
            bmp.CopyPixels(pixels, bmp.PixelWidth * 4, 0);

            int padding = bmp.PixelWidth % 32;

            var bits = new Bits();
            for (int y = bmp.PixelHeight - 1; y >= 0; --y)
            {
                for (int x = 0; x < bmp.PixelWidth; ++x)
                {
                    int alpha = (pixels[y * bmp.PixelWidth + x] >> 24) & 0xff;
                    bits.Add(alpha <= 0 ? 1 : 0);
                }
                // padding
                for (int i=0; i< padding; ++i)
                {
                    bits.Add(0);
                }
            }

            this.Mask = bits.Buffer.ToArray();

            return this.Mask;
        }
    }
}
