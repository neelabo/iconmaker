﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IconMaker
{

    /// <summary>
    /// SimpleSlider.xaml の相互作用ロジック
    /// </summary>
    public partial class SimpleSlider : UserControl
    {
        public event EventHandler DragStarted;
        public event EventHandler DragCompleted;

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
            "Value",
            typeof(double),
            typeof(SimpleSlider),
            new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(OnValueChanged)));

        public double Value
        {
            get
            {
                double v = (double)GetValue(ValueProperty);
                if (v < Minimum) v = Minimum;
                if (v > Maximum) v = Maximum;
                return v;
            }
            set
            {
                double v = value;
                if (v < Minimum) v = Minimum;
                if (v > Maximum) v = Maximum;
                SetValue(ValueProperty, v);
            }
        }

        private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((SimpleSlider)obj).UpdateX();
        }


        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register(
            "Minimum",
            typeof(double),
            typeof(SimpleSlider),
            new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(OnMinimumChanged)));

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set
            {
                SetValue(MinimumProperty, value);
            }
        }

        private static void OnMinimumChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((SimpleSlider)obj).UpdateX();
        }


        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register(
            "Maximum",
            typeof(double),
            typeof(SimpleSlider),
            new FrameworkPropertyMetadata(1.0, new PropertyChangedCallback(OnMaximumChanged)));

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        private static void OnMaximumChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((SimpleSlider)obj).UpdateX();
        }

        void UpdateX()
        {
            double x = (Value - Minimum) * ActualWidth / (Maximum - Minimum) - 5.5;
            Canvas.SetLeft(Thumb, x);
        }


        //
        public SimpleSlider()
        {
            InitializeComponent();
        }



        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateX();
        }


        private void Thumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            DragStarted?.Invoke(this, null);
        }

        private void Thumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var x = Canvas.GetLeft(Thumb) + e.HorizontalChange;

            double v = (x + 5.5) * (Maximum - Minimum) / ActualWidth + Minimum;
            Value = v;
        }

        private void Thumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            DragCompleted.Invoke(this, null);
        }
    }


}
