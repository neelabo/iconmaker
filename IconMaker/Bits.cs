﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IconMaker
{
    public class Bits
    {
        public int Length { private set; get; }

        public List<byte> Buffer { private set; get; } = new List<byte>();

        struct Address
        {
            public int block;
            public int offset;

            public Address(int pos)
            {
                block = pos / 8;
                offset = 7 - pos % 8;
            }
        };

        //
        public int this[int index]
        {
            set { Set(index, value); }
            get { return Get(index); }
        }

        //
        public void Clear()
        {
            Length = 0;
            Buffer.Clear();
        }

        //
        private void Set(Address address, int value)
        {
            Buffer[address.block] = (byte)(Buffer[address.block] & ~(1 << address.offset) | ((value == 0 ? 0 : 1) << address.offset));
        }

        public void Set(int pos, int value)
        {
            Set(new Address(pos), value);
        }


        //
        private int Get(Address address)
        {
            return (Buffer[address.block] >> address.offset) & 0x01;
        }

        public int Get(int pos)
        {
            return Get(new Address(pos));
        }

        //
        public void Add(int value)
        {
            Length++;
            var address = new Address(Length-1);
            if (Buffer.Count <= address.block)
            {
                Buffer.Add(0x00); // + 1byte
            }
            Set(address, value);
        }

        //
        public void AddByte(byte value)
        {
            for (int i = 7; i >= 0; --i) 
            {
                Add(value & (1 << i));
            }
        }

        //
        public void AddInt32(int value)
        {
            for (int i = 31; i >= 0; --i)
            {
                Add(value & (1 << i));
            }
        }
    }
}
