﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using HL.CSharp.Wpf.Icons;
using IconMaker.Layers;
using System.Windows.Input;

namespace IconMaker
{
    /// <summary>
    /// Icon Generator
    /// </summary>
    public class IconGenerator : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([global::System.Runtime.CompilerServices.CallerMemberNameAttribute] string name = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
            }
        }
        #endregion

        public ObservableCollection<IconSource> IconSources { get; set; }

        public ObservableCollection<Layer> Layers { get; set; }

        public static IconGenerator Instance { get; private set; }

        #region Property: SelectedLayer
        private Layer _SelectedLayer;
        public Layer SelectedLayer
        {
            get { return _SelectedLayer; }
            set { _SelectedLayer = value; OnPropertyChanged(); UpdateEditLayer(); }
        }
        #endregion

        #region Property: EditLayer
        private Layer _EditLayer;
        public Layer EditLayer
        {
            get { return _EditLayer; }
            set { _EditLayer = value; OnPropertyChanged(); OnPropertyChanged("EditLevelLabel"); }
        }
        #endregion


        void UpdateEditLayer()
        {
            EditLayer = SelectedLayer?.EditLayer;
        }

        public string EditLevelLabel { get { return EditLayer == null ? "" : IconSources[EditLayer.Level].Label; } }


        LayerFactory LayerFactory;

        private int _LayerSerialNumber;


        public ICommand CommandCopy { get; set; }
        public ICommand CommandMoveUp { get; set; }
        public ICommand CommandMoveDown { get; set; }
        public ICommand CommandRemove { get; set; }


        public IconGenerator()
        {
            Instance = this;

            LayerFactory = new LayerFactory();

            IconSources = new ObservableCollection<IconSource>();

            IconSources.Add(new IconSource() { Level = 0, Pixel = 256 }); // default
            IconSources.Add(new IconSource() { Level = 1, Pixel = 256 });
            IconSources.Add(new IconSource() { Level = 2, Pixel = 48 });
            IconSources.Add(new IconSource() { Level = 3, Pixel = 32 });
            IconSources.Add(new IconSource() { Level = 4, Pixel = 16 });

            Layers = new ObservableCollection<Layer>();

            Layers.CollectionChanged += Layers_CollectionChanged;

            CommandCopy = new RelayCommand(() => { if (SelectedLayer != null) CopyLayer(SelectedLayer); });
            CommandRemove = new RelayCommand(() => { if (SelectedLayer != null) RemoveLayer(SelectedLayer); } );
            CommandMoveUp = new RelayCommand(() => { if (SelectedLayer != null) MoveUpLayer(SelectedLayer); });
            CommandMoveDown = new RelayCommand(() => { if (SelectedLayer != null) MoveDownLayer(SelectedLayer); });
        }

        private void Layers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            CreateLeveImages();
        }


        public void Initialize()
        {
            SelectedLayer = null;

            Layers.Clear();
            _LayerSerialNumber = 0;

            NewLayer(LayerType.Rectangle);

#if DEBUG
            var textLayer = (LayerProxy<TextLayer>)NewLayer(LayerType.Text);
            textLayer.Default.Text.Value = "ABC";
#endif

            SelectedLayer = Layers[0];

            App.History.Clear();
        }

        //
        public string UniqueLabelNum(string label, int count=1)
        {
            while (Layers.Any(e => e.Label == label))
            {
                var regex = new global::System.Text.RegularExpressions.Regex((System.String)@"\s*\d+$");
                label = regex.Replace(label, "");
                label += $" {++count}";
            }

            return label;
        }

        //
        public int UniqueID()
        {
            int count = 1;
            while (Layers.Any(e => e.ID == count))
            {
                count++;
            }

            return count;
        }

        // 新規レイヤ追加
        public Layer NewLayer(LayerType type)
        {
            App.History.Lock();
            Layer layer = LayerFactory.Create(type);
            layer.Label = UniqueLabelNum($"Layer {++_LayerSerialNumber}", _LayerSerialNumber);
            layer.ID = UniqueID();
            App.History.Unlock();

            AddLayer(layer);

            App.History.Add(new MementoLayerAdd(this, layer));

            return layer;
        }

        // レイヤ追加
        public void AddLayer(Layer layer)
        {
            InitializeLayerEvent(layer);
            Layers.Insert(0, layer);

            SetEditLevel(_EditLevel);
            SelectedLayer = layer;
        }

        // レイヤ削除
        public void RemoveLayer(Layer layer)
        {
            App.History.BeginGroup();
            App.History.Add(new MementoLayerRemove(this, layer));

            Layers.Remove(layer);
            SelectedLayer = null;

            RepareLayers();

            App.History.EndGroup();
        }

        // レイヤ初期化
        // イベントの登録
        private void InitializeLayerEvent(Layer layer)
        {
            layer.PropertyChanged += Layer_PropertyChanged;
        }

        // レイヤのサムネイルが変更されたらアイコン画像も更新する
        private void Layer_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Thumbnail")
            {
                CreateLeveImages();
            }
        }

        // レイヤの複製
        public void CopyLayer(Layer layer)
        {
            App.History.Lock();
            var newLayer = layer.Clone();
            newLayer.Label = newLayer.Label + "'";
            newLayer.ID = UniqueID();
            App.History.Unlock();

            int index = Layers.IndexOf(layer) + 1;
            if (index > Layers.Count) index = Layers.Count;
            Layers.Insert(index, newLayer);
            InitializeLayerEvent(newLayer);

            App.History.Add(new MementoLayerAdd(this, newLayer));

            SetEditLevel(_EditLevel);
            SelectedLayer = newLayer;
        }

        // レイヤを上に移動する
        public void MoveUpLayer(Layer layer)
        {
            int index = Layers.IndexOf(layer);
            if (index >= 1)
            {
                Layers.Move(index, index - 1);

                App.History.Add(new MementoLayerMove(this, index, index - 1));
            }
        }

        // レイヤを下に移動する
        public void MoveDownLayer(Layer layer)
        {
            int index = Layers.IndexOf(layer);
            if (index >= 0 && index < Layers.Count - 1)
            {
                Layers.Move(index, index + 1);

                App.History.Add(new MementoLayerMove(this, index, index + 1));
            }
        }

        // レイヤを移動する
        public void MoveIndexLayer(Layer layer, int newIndex)
        {
            int oldIndex = Layers.IndexOf(layer);
            if (oldIndex == newIndex) return;
            Layers.Move(oldIndex, newIndex);
            App.History.Add(new MementoLayerMove(this, oldIndex, newIndex));
        }



        // ##
        Vector _OldLayerPosition;
        double _OldAngle;
        Vector _OldLayerSize;

        // マウスによるレイヤの操作：開始
        public void MoveLayerStart(Point start)
        {
            if (SelectedLayer == null) return;

            _OldLayerPosition = new Vector(SelectedLayer.Common.Left.Value, SelectedLayer.Common.Top.Value);
            _OldAngle = SelectedLayer.Common.Angle.Value;
            _OldLayerSize = new Vector(SelectedLayer.Common.Width.Value, SelectedLayer.Common.Height.Value);

            App.History.BeginGroup();
        }

        // マウスによるレイヤの座標移動：移動中
        public void MoveLayer(Point start, Point end)
        {
            if (SelectedLayer == null) return;

            var pos = _OldLayerPosition + end - start;
            SelectedLayer.Common.Top.Value = pos.Y;
            SelectedLayer.Common.Left.Value = pos.X;
        }

        // レイヤの座標移動
        // 方向キーによる移動に使用される
        public void MoveLayer(double dx, double dy)
        {
            if (SelectedLayer == null) return;

            SelectedLayer.Common.Top.Value += dy;
            SelectedLayer.Common.Left.Value += dx;
        }

        // マウスによるレイヤの操作：終了
        public void MoveLayerEnd()
        {
            if (SelectedLayer == null) return;

            App.History.EndGroup();
        }

        // マウスに依るレイヤの回転：回転中
        public void AngleLayer(Point start, Point end)
        {
            if (SelectedLayer == null) return;

            var center = new Vector(_OldLayerPosition.X + _OldLayerSize.X * 0.5, _OldLayerPosition.Y + _OldLayerSize.X * 0.5);

            Vector v0 = (Vector)start - center;
            Vector v1 = (Vector)end - center;

            double angle = Vector.AngleBetween(v0, v1);
            SelectedLayer.Common.Angle.Value = Math.Floor(NormalizeLoopRange(_OldAngle + angle, -180, 180));
        }

        // 角度の正規化
        private double NormalizeLoopRange(double val, double min, double max)
        {
            if (min >= max) throw new global::System.ArgumentException((System.String)"need min < max");

            if (val >= max)
            {
                return min + (val - min) % (max - min);
            }
            else if (val < min)
            {
                return max - (min - val) % (max - min);
            }
            else
            {
                return val;
            }
        }

        // マウスに依るサイズの拡縮：変更中
        public void SizeLayer(Point start, Point end)
        {
            var center = new Vector(_OldLayerPosition.X + _OldLayerSize.X * 0.5, _OldLayerPosition.Y + _OldLayerSize.X * 0.5);

            Vector v0 = (Vector)start - center;
            Vector v1 = (Vector)end - center;

            double scale = v1.Length / v0.Length;

            double dx = _OldLayerSize.X * scale - _OldLayerSize.X;
            double dy = _OldLayerSize.Y * scale - _OldLayerSize.Y;

            SelectedLayer.Common.Left.Value = Math.Floor(_OldLayerPosition.X - dx * 0.5);
            SelectedLayer.Common.Top.Value = Math.Floor(_OldLayerPosition.Y - dy * 0.5);
            SelectedLayer.Common.Width.Value = Math.Floor(_OldLayerSize.X + dx);
            SelectedLayer.Common.Height.Value = Math.Floor(_OldLayerSize.Y + dy);
        }



        private int _EditLevel;

        // レベルの選択
        public void SetEditLevel(int level)
        {
            _EditLevel = level;

            foreach (var layer in Layers)
            {
                var layerProxy = layer as ILayerProxy;
                layerProxy?.SetLevel(level);
            }

            UpdateEditLayer();
        }


      
        // アイコン画像をレンダリングするコントロール作成
        public Canvas CreateCanvas(int level)
        {
            var canvas = new Canvas();
            canvas.Width = 256;
            canvas.Height = 256;

            foreach (var layer in Layers.Reverse())
            {
                canvas.Children.Add(layer.CreateMaskedCanvasUIElement(level));
            }

            return canvas;
        }


        // アイコン画像作成
        public BitmapSource CreateImageSource(int level, int pixel)
        {
            var canvas = CreateCanvas(level);

            // ビューツリー外でも正常にレンダリングするようにする処理
            canvas.Measure(new Size(canvas.Width, canvas.Height));
            canvas.Arrange(new Rect(new Size(canvas.Width, canvas.Height)));
            canvas.UpdateLayout();

            double dpi = 96.0 * pixel / canvas.Width;

            RenderTargetBitmap bmp = new RenderTargetBitmap(pixel, pixel, dpi, dpi, PixelFormats.Pbgra32);
            bmp.Render(canvas);

            return bmp;
        }


        // すべてのレベルのアイコン画像作成
        public void CreateLeveImages()
        {
            foreach (var iconSource in IconSources)
            {
                iconSource.Image = CreateImageSource(iconSource.Level, iconSource.Pixel);
            }
        }


        // セーブデータ
        [DataContract]
        [KnownType(typeof(LayerProxy<TextLayer>))]
        [KnownType(typeof(LayerProxy<RectangleLayer>))]
        [KnownType(typeof(LayerProxy<EllipseLayer>))]
        [KnownType(typeof(LayerProxy<BitmapLayer>))]
        [KnownType(typeof(LayerProxy<SvgLayer>))]
        public class SaveData
        {
            [DataMember]
            public string ID { get; set; } = "NeeLaboratory.MakeIcon Format=1.3";

            [DataMember]
            public ObservableCollection<Layer> Layers { get; set; }
        }

        // セーブ
        public void Save(string path)
        {
            var saveData = new SaveData();
            saveData.Layers = Layers;

            var settings = new global::System.Xml.XmlWriterSettings();
            settings.Encoding = new global::System.Text.UTF8Encoding((System.Boolean)false);
            //settings.Indent = true;
            using (var xw = System.Xml.XmlWriter.Create(path, settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(SaveData));
                serializer.WriteObject(xw, saveData);
            }

            App.History.ResetChangeFlag();
        }

        // ロード
        public void Load(string path)
        {
            using (var reader = System.Xml.XmlReader.Create(path))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(SaveData));
                SaveData saveData = (SaveData)ser.ReadObject(reader, true);

                Layers = saveData.Layers;
                RecoveryLayers();
                OnPropertyChanged("Layers");
            }
        }

        // ロード後のリカバリ処理
        void RecoveryLayers()
        {
            _LayerSerialNumber = 0;

            RepareLayers();

            foreach (var layer in Layers)
            {
                InitializeLayerEvent(layer);
            }

            Layers.CollectionChanged += Layers_CollectionChanged;

            App.History.Clear();
        }

        // レイヤ修復(レイヤのリンク切れ等)
        void RepareLayers()
        {
            foreach (var layer in Layers.Reverse())
            {
                layer.Repare();
            }
        }

        // export ICO
        public void ExportIcon(string path)
        {
            using (global::System.IO.FileStream fs = new global::System.IO.FileStream((System.String)path, (global::System.IO.FileMode)global::System.IO.FileMode.Create))
            {
                var encoder = new IconBitmapEncoder();

                foreach (var iconSource in IconSources)
                {
                    if (iconSource.Level == 0) continue;
                    if (iconSource.Pixel >= 256)
                    {
                        encoder.Frames.Add(IconFrame.Create((BitmapFrame)BitmapFrame.Create((BitmapSource)iconSource.Image), (System.Byte[])null));
                    }
                    else
                    {
                        encoder.Frames.Add(IconFrame.Create((BitmapFrame)BitmapFrame.Create((BitmapSource)IconBitmapEncoder.Get24plus8BitImage((BitmapSource)iconSource.Image)), (System.Byte[])(new BitmapMask((BitmapSource)iconSource.Image)).Mask));
                    }
                }

                encoder.Save(fs);
            }
        }

        // export PNG
        public void ExportPng(string path, int level)
        {
            int pixel = IconSources[level].Pixel;

            BitmapSource source = CreateImageSource(level, pixel);

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(source));

            //
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            using (var fs = System.IO.File.Open(path, System.IO.FileMode.Create))
            {
                encoder.Save(fs);
            }
        }
    }

    /// <summary>
    /// MementoCommand LayerAdd
    /// </summary>
    class MementoLayerAdd : MementoCommand<IconGenerator, Layer>
    {
        int _Position;

        public MementoLayerAdd(IconGenerator target, Layer layer) : base(target, null, layer)
        {
            _Position = target.Layers.IndexOf(layer);
        }

        public override void Redo()
        {
            Target.Layers.Insert(_Position, Value1);
        }

        public override void Undo()
        {
            Target.Layers.Remove(Value1);
            if (Target.SelectedLayer == Value1) Target.SelectedLayer = null;
        }
    }

    /// <summary>
    /// MementoCommand LayerRemove
    /// </summary>
    class MementoLayerRemove : MementoCommand<IconGenerator, Layer>
    {
        int _Position;

        public MementoLayerRemove(IconGenerator target, Layer layer) : base(target, layer, null)
        {
            _Position = target.Layers.IndexOf(layer);
        }

        public override void Redo()
        {
            Target.Layers.Remove(Value0);
            if (Target.SelectedLayer == Value0) Target.SelectedLayer = null;
        }

        public override void Undo()
        {
            Target.Layers.Insert(_Position, Value0);
        }
    }

    /// <summary>
    /// MementoCommand LayerMove
    /// </summary>
    class MementoLayerMove : MementoCommand<IconGenerator, int>
    {
        public MementoLayerMove(IconGenerator target, int v0, int v1) : base(target, v0, v1)
        {
        }

        public override void Redo()
        {
            Target.Layers.Move(Value0, Value1);
        }

        public override void Undo()
        {
            Target.Layers.Move(Value1, Value0);
        }
    }
}
