﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IconMaker.Layers
{
    public enum LayerType
    {
        Text,
        Rectangle,
        Ellipse,
        Bitmap,
        Svg,
    }

    public class LayerFactory
    {
        Dictionary<LayerType, Type> Types;

        public LayerFactory()
        {
            Types = new Dictionary<LayerType, Type>();

            Types[LayerType.Text] = typeof(LayerProxy<TextLayer>);
            Types[LayerType.Rectangle] = typeof(LayerProxy<RectangleLayer>);
            Types[LayerType.Ellipse] = typeof(LayerProxy<EllipseLayer>);
            Types[LayerType.Bitmap] = typeof(LayerProxy<BitmapLayer>);
            Types[LayerType.Svg] = typeof(LayerProxy<SvgLayer>);
        }

        public Layer Create(LayerType type)
        {
            return (Layer)Activator.CreateInstance(Types[type]);
        }
    }
}
