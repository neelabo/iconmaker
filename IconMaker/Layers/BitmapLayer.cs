﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IconMaker.Layers
{

    [DataContract]
    public class BitmapLayer : Layer
    {
        public override string TypeName { get { return "Bitmap"; } }

        [DataMember]
        public override CommonParameter Common { get; set; }

        [DataMember]
        public LayerParameterBitmap Path { get; set; }
        [DataMember]
        public LayerParameterDouble Opacity { get; set; }


        [DataMember]
        public EffectParameter Effect { get; set; }

        //
        public BitmapLayer()
        {
            Constructor();
            InitializeParameterMap();
        }

        private void Constructor()
        {
            Common = new CommonParameter();

            Path = new LayerParameterBitmap();
            Opacity = new LayerParameterDouble() { Value = 1.0 };

            Effect = new EffectParameter();
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext c)
        {
            Constructor();
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            InitializeParameterMap();
        }

        private void InitializeParameterMap()
        {
            ParameterMap = new Dictionary<string, LayerParameter>();

            Common.InitializeParameterMap(this);

            ParameterMap[nameof(Path)] = Path;
            ParameterMap[nameof(Opacity)] = Opacity;

            Effect.InitializeParameterMap(nameof(Effect), this);

            base.InitializeParameterMapEvent();
        }

        public override UIElement CreatePropertyEditerUIElement()
        {
            var stackPanel = new global::System.Windows.Controls.StackPanel();

            stackPanel.Children.Add(Common.ParameterControl());

            stackPanel.Children.Add(new Separator());

            stackPanel.Children.Add(ParameterControls.FileName("Image", Path, OpenFileDialogTools.CreateOpenPictureFileDialog()));
            stackPanel.Children.Add(ParameterControls.Percent("Opacity", Opacity));

            stackPanel.Children.Add(new Separator());

            stackPanel.Children.Add(Effect.ParameterControl());

            return stackPanel;
        }

        public override UIElement CreateCanvasUIElement(int level)
        {
            var image = new Image();

            Common.SetFrameworkElementParameter(image, level);

            image.Source = Path.Bitmap;
            image.Opacity = Opacity.Value;

            image.Effect = Effect.CreateCanvasUIElement(level);

            return image;
        }

    }


    public static class OpenFileDialogTools
    {
        public static OpenFileDialog CreateOpenPictureFileDialog()
        {
            var dialog = new OpenFileDialog();

            dialog = new OpenFileDialog();
            dialog.Title = "ピクチャファイルの読み込み";
            dialog.Filter = "BMP|*.bmp;*.dib|JPEG|*.jpg;*.jpeg;*.jpe;*.jfif|GIF|*.gif|TIFF|*.tif;*.tiff|PNG|*.png|ICO|*.ico"
                + "|All Picture Files|*.bmp;*.dib;*.jpg;*.jpeg;*.jpe;*.jfif;*.gif;*.tif;*.tiff;*.png;*.ico"
                + "|All Files|*.*";
            dialog.FilterIndex = 7;

            return dialog;
        }

        public static OpenFileDialog CreateOpenSvgFileDialog()
        {
            var dialog = new OpenFileDialog();

            dialog = new OpenFileDialog();
            dialog.Title = "SVGファイルの読み込み";
            dialog.Filter = "SVG|*.svg|All Files|*.*";
            dialog.FilterIndex = 1;

            return dialog;
        }
    }


    /// <summary>
    /// ParameterBitmap
    /// </summary>
    [DataContract]
    public class LayerParameterBitmap : LayerParameterPath
    {
        private LayerParameterBitmap _Parent => (LayerParameterBitmap)Parent;

        private BitmapImage _Bitmap;
        public BitmapImage Bitmap
        {
            get { return IsOverride || Parent == null ? _Bitmap : _Parent.Bitmap; }
            set { _Bitmap = value; }
        }

        [OnDeserialized]
        private void Deserializing(StreamingContext c)
        {
            Load(Value);
        }


        //
        protected override void SetPrivateValue(string value)
        {
            Load(value);
            base.SetPrivateValue(value);
        }

        //
        private void Load(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                Bitmap = null;
                IsValid = false;
                return;
            }

            try
            {
                var source = new BitmapImage();
                source.BeginInit();
                source.UriSource = new Uri(path);
                source.EndInit();

                Bitmap = source;
                IsValid = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Bitmap = null;
                IsValid = false;
            }
        }
    }


}
