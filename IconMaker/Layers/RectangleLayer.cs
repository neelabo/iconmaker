﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace IconMaker.Layers
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class RectangleLayer : Layer
    {
        public override string TypeName { get { return "Rectangle"; } }

        [DataMember]
        public override CommonParameter Common { set; get; }

        [DataMember]
        public BrushParameter Brush { get; set; }

        [DataMember]
        public LayerParameterDouble RadiusX { set; get; }
        [DataMember]
        public LayerParameterDouble RadiusY { set; get; }
        [DataMember]
        public LayerParameterDouble StrokeThickness { set; get; }
        [DataMember]
        public BrushParameter StrokeBrush { set; get; }

        [DataMember]
        public EffectParameter Effect { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RectangleLayer()
        {
            Common = new CommonParameter();

            Brush = new BrushParameter();
            Brush.Brush.Value = 1;
            RadiusX = new LayerParameterDouble();
            RadiusY = new LayerParameterDouble();
            StrokeThickness = new LayerParameterDouble() { Value = 8 };
            StrokeBrush = new BrushParameter();

            Effect = new EffectParameter();


            InitializeParameterMap();
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            InitializeParameterMap();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeParameterMap()
        {
            ParameterMap = new Dictionary<string, LayerParameter>();

            Common.InitializeParameterMap(this);

            Brush.InitializeParameterMap(nameof(Brush), this);

            ParameterMap[nameof(RadiusX)] = RadiusX;
            ParameterMap[nameof(RadiusY)] = RadiusY;
            ParameterMap[nameof(StrokeThickness)] = StrokeThickness;
            StrokeBrush.InitializeParameterMap(nameof(StrokeBrush), this);
            //StrokeBrush.layer = this;

            Effect.InitializeParameterMap(nameof(Effect), this);

            base.InitializeParameterMapEvent();
        }


        public override UIElement CreatePropertyEditerUIElement()
        {
            var stackPanel = new global::System.Windows.Controls.StackPanel();

            stackPanel.Children.Add(Common.ParameterControl());

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(Brush.ParameterControl("FillBrush"));
            stackPanel.Children.Add(ParameterControls.Double("RadiusX", RadiusX));
            stackPanel.Children.Add(ParameterControls.Double("RadiusY", RadiusY));

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(StrokeBrush.ParameterControl("StrokeBrush"));
            stackPanel.Children.Add(ParameterControls.Double("StrokeThickness", StrokeThickness));

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(Effect.ParameterControl());

            return stackPanel;
        }


        ///
        public override void UpdateThumbnail()
        {
            // ブラシ更新
            Brush.UpdateBrush();
            StrokeBrush.UpdateBrush();

            base.UpdateThumbnail();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public override UIElement CreateCanvasUIElement(int level)
        {
            var rectangle = new Rectangle();

            Common.SetFrameworkElementParameter(rectangle, level);

            rectangle.RadiusX = RadiusX.Value;
            rectangle.RadiusY = RadiusY.Value;

            rectangle.Fill = Brush.CreateCanvasUIElement(level);

            rectangle.Stroke = StrokeBrush.CreateCanvasUIElement(level);
            rectangle.StrokeThickness = StrokeThickness.Value;

            rectangle.Effect = Effect.CreateCanvasUIElement(level);

            return rectangle;
        }
    }
}
