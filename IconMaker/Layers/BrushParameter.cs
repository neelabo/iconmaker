﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace IconMaker.Layers
{
    [DataContract]
    public class BrushParameter : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([global::System.Runtime.CompilerServices.CallerMemberNameAttribute] string name = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
            }
        }
        #endregion

        [DataMember]
        public LayerParameterBrush Brush { get; set; }
        [DataMember]
        public LayerParameterColor SolidColor { get; set; }
        [DataMember]
        public LayerParameterDouble SolidColorOpacity { get; set; }

        [DataMember]
        public LayerParameterColor LinearGradientColor1 { get; set; }
        [DataMember]
        public LayerParameterDouble LinearGradientOffset1 { get; set; }
        [DataMember]
        public LayerParameterColor LinearGradientColor2 { get; set; }
        [DataMember]
        public LayerParameterDouble LinearGradientOffset2 { get; set; }
        [DataMember]
        public LayerParameterDouble LinearGradientStartPointX { get; set; }
        [DataMember]
        public LayerParameterDouble LinearGradientStartPointY { get; set; }
        [DataMember]
        public LayerParameterDouble LinearGradientEndPointX { get; set; }
        [DataMember]
        public LayerParameterDouble LinearGradientEndPointY { get; set; }
        [DataMember]
        public LayerParameterDouble LinearGradientOpacity { get; set; }


        [DataMember]
        public LayerParameterColor RadialGradientColor1 { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientOffset1 { get; set; }
        [DataMember]
        public LayerParameterColor RadialGradientColor2 { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientOffset2 { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientOriginX { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientOriginY { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientCenterX { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientCenterY { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientRadiusX { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientRadiusY { get; set; }
        [DataMember]
        public LayerParameterDouble RadialGradientOpacity { get; set; }

        [DataMember]
        public LayerParameterBitmap ImagePath { get; set; }
        [DataMember]
        public LayerParameterDouble ImageOpacity { get; set; }


        #region Property: SampleBrush
        private Brush _SampleBrush;
        public Brush SampleBrush
        {
            get { return _SampleBrush; }
            set { _SampleBrush = value; OnPropertyChanged(); }
        }
        #endregion


        // parameter map.
        private Dictionary<string, LayerParameter> _BrushParameterMap;

        //  
        public BrushParameter()
        {
            Brush = new LayerParameterBrush();

            SolidColor = new LayerParameterColor() { Value = "#FF000000" };
            SolidColorOpacity = new LayerParameterDouble() { Value = 1.0 };

            LinearGradientColor1 = new LayerParameterColor() { Value = "#FF000000" };
            LinearGradientOffset1 = new LayerParameterDouble() { Value = 0.0 };
            LinearGradientColor2 = new LayerParameterColor() { Value = "#FFFFFFFF" };
            LinearGradientOffset2 = new LayerParameterDouble() { Value = 1.0 };
            LinearGradientStartPointX = new LayerParameterDouble() { Value = 0.5 };
            LinearGradientStartPointY = new LayerParameterDouble() { Value = 0 };
            LinearGradientEndPointX = new LayerParameterDouble() { Value = 0.5 };
            LinearGradientEndPointY = new LayerParameterDouble() { Value = 1 };
            LinearGradientOpacity = new LayerParameterDouble() { Value = 1.0 };


            RadialGradientColor1 = new LayerParameterColor() { Value = "#FF000000" };
            RadialGradientOffset1 = new LayerParameterDouble() { Value = 0.0 };
            RadialGradientColor2 = new LayerParameterColor() { Value = "#FFFFFFFF" };
            RadialGradientOffset2 = new LayerParameterDouble() { Value = 1.0 };
            RadialGradientOriginX = new LayerParameterDouble() { Value = 0.5 };
            RadialGradientOriginY = new LayerParameterDouble() { Value = 0.5 };
            RadialGradientCenterX = new LayerParameterDouble() { Value = 0.5 };
            RadialGradientCenterY = new LayerParameterDouble() { Value = 0.5 };
            RadialGradientRadiusX = new LayerParameterDouble() { Value = 0.5 };
            RadialGradientRadiusY = new LayerParameterDouble() { Value = 0.5 };
            RadialGradientOpacity = new LayerParameterDouble() { Value = 1.0 };

            ImagePath = new LayerParameterBitmap();
            ImageOpacity = new LayerParameterDouble() { Value = 1.0 };
        }

        public void InitializeParameterMap(string label, Layer layer)
        {
            //this.layer = layer;

            _BrushParameterMap = new Dictionary<string, LayerParameter>();

            _BrushParameterMap[nameof(Brush)] = Brush;

            _BrushParameterMap[nameof(SolidColor)] = SolidColor;
            _BrushParameterMap[nameof(SolidColorOpacity)] = SolidColorOpacity;

            _BrushParameterMap[nameof(LinearGradientColor1)] = LinearGradientColor1;
            _BrushParameterMap[nameof(LinearGradientOffset1)] = LinearGradientOffset1;
            _BrushParameterMap[nameof(LinearGradientColor2)] = LinearGradientColor2;
            _BrushParameterMap[nameof(LinearGradientOffset2)] = LinearGradientOffset2;
            _BrushParameterMap[nameof(LinearGradientStartPointX)] = LinearGradientStartPointX;
            _BrushParameterMap[nameof(LinearGradientStartPointY)] = LinearGradientStartPointY;
            _BrushParameterMap[nameof(LinearGradientEndPointX)] = LinearGradientEndPointX;
            _BrushParameterMap[nameof(LinearGradientEndPointY)] = LinearGradientEndPointY;
            _BrushParameterMap[nameof(LinearGradientOpacity)] = LinearGradientOpacity;

            _BrushParameterMap[nameof(RadialGradientColor1)] = RadialGradientColor1;
            _BrushParameterMap[nameof(RadialGradientOffset1)] = RadialGradientOffset1;
            _BrushParameterMap[nameof(RadialGradientColor2)] = RadialGradientColor2;
            _BrushParameterMap[nameof(RadialGradientOffset2)] = RadialGradientOffset2;
            _BrushParameterMap[nameof(RadialGradientOriginX)] = RadialGradientOriginX;
            _BrushParameterMap[nameof(RadialGradientOriginY)] = RadialGradientOriginY;
            _BrushParameterMap[nameof(RadialGradientCenterX)] = RadialGradientCenterX;
            _BrushParameterMap[nameof(RadialGradientCenterY)] = RadialGradientCenterY;
            _BrushParameterMap[nameof(RadialGradientRadiusX)] = RadialGradientRadiusX;
            _BrushParameterMap[nameof(RadialGradientRadiusY)] = RadialGradientRadiusY;
            _BrushParameterMap[nameof(RadialGradientOpacity)] = RadialGradientOpacity;

            _BrushParameterMap[nameof(ImagePath)] = ImagePath;
            _BrushParameterMap[nameof(ImageOpacity)] = ImageOpacity;

            foreach (var pair in _BrushParameterMap)
            {
                if (Brush == pair.Value)
                {
                    layer.ParameterMap[label] = pair.Value;
                }
                else 
                {
                    layer.ParameterMap[label + "." + pair.Key] = pair.Value;
                    Brush.AddOverrideChild(pair.Value);
                }
                pair.Value.PropertyChanged += Value_PropertyChanged;
            }
        }


        private void Value_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Brush.Brush = null;
            Brush.SampleBrush = null;
        }

        public UIElement ParameterControl(string label)
        {
            var items = new List<TabItem>();
            TabItem item;
            StackPanel stack;

            // None
            item = new TabItem();
            item.Header = "None";
            items.Add(item);

            // Solid Color
            item = new TabItem();
            item.Header = "Solid";
            stack = new StackPanel();
            stack.Children.Add(ParameterControls.Color("Color", SolidColor));
            stack.Children.Add(ParameterControls.Percent("Opacity", SolidColorOpacity));
            item.Content = stack;
            items.Add(item);

            // Linear Gradient
            item = new TabItem();
            item.Header = "LinearGrad";
            stack = new StackPanel();

            var gradientParams = new GradientParameter[]
                {
                    new GradientParameter() { Color = LinearGradientColor1, Offset = LinearGradientOffset1 },
                    new GradientParameter() { Color = LinearGradientColor2, Offset = LinearGradientOffset2 },
                };
            stack.Children.Add(ParameterGradientSlider(gradientParams));

            stack.Children.Add(ParameterControls.Color("Color1", LinearGradientColor1));
            stack.Children.Add(ParameterControls.Color("Color2", LinearGradientColor2));

            stack.Children.Add(ParameterControls.DoubleDouble("StartPoint", LinearGradientStartPointX, LinearGradientStartPointY));
            stack.Children.Add(ParameterControls.DoubleDouble("EndPoint", LinearGradientEndPointX, LinearGradientEndPointY));

            stack.Children.Add(ParameterControls.Percent("Opacity", LinearGradientOpacity));

            item.Content = stack;
            items.Add(item);

            // Radial Gradient
            item = new TabItem();
            item.Header = "RadialGrad";
            stack = new StackPanel();

            var gradientParamsRadial = new GradientParameter[]
                {
                    new GradientParameter() { Color = RadialGradientColor1, Offset = RadialGradientOffset1 },
                    new GradientParameter() { Color = RadialGradientColor2, Offset = RadialGradientOffset2 },
                };
            stack.Children.Add(ParameterGradientSlider(gradientParamsRadial));

            stack.Children.Add(ParameterControls.Color("Color1", RadialGradientColor1));
            stack.Children.Add(ParameterControls.Color("Color2", RadialGradientColor2));

            stack.Children.Add(ParameterControls.DoubleDouble("GradientOrigin", RadialGradientOriginX, RadialGradientOriginY));
            stack.Children.Add(ParameterControls.DoubleDouble("Center", RadialGradientCenterX, RadialGradientCenterY));

            stack.Children.Add(ParameterControls.Double("RadiusX", RadialGradientRadiusX));
            stack.Children.Add(ParameterControls.Double("RadiusY", RadialGradientRadiusY));

            stack.Children.Add(ParameterControls.Percent("Opacity", RadialGradientOpacity));

            item.Content = stack;
            items.Add(item);

            // Image Brush
            item = new TabItem();
            item.Header = "Image";
            stack = new StackPanel();

            stack.Children.Add(ParameterControls.FileName("Image", ImagePath, OpenFileDialogTools.CreateOpenPictureFileDialog()));
            stack.Children.Add(ParameterControls.Percent("Opacity", ImageOpacity));

            item.Content = stack;
            items.Add(item);

            //TabControl tabControl;
            return ParameterControls.Tab(label, Brush, items);
        }

        //
        public class GradientParameter
        {
            public LayerParameterColor Color;
            public LayerParameterDouble Offset;
        }


        //
        public UIElement ParameterGradientSlider(GradientParameter[] parameters)
        {
            var grid = new Grid();
            grid.Height = 40;
            grid.Margin = new Thickness(5);

            var border = new Border();
            border.Height = 20;
            border.VerticalAlignment = VerticalAlignment.Top;

            UpdateBrush();
            Binding bindingBrush = new Binding(nameof(SampleBrush)); //  "SampleBrush");
            bindingBrush.Source = this;
            border.SetBinding(Border.BackgroundProperty, bindingBrush);

            grid.Children.Add(border);


            foreach (var param in parameters)
            {
                var slider = new SimpleSlider();
                slider.Margin = new Thickness(0, 20, 0, 0);
                slider.Minimum = 0.0;
                slider.Maximum = 1.0;
                Binding bindingColor = new Binding("Value");
                bindingColor.Source = param.Color;
                bindingColor.Mode = BindingMode.TwoWay;
                slider.SetBinding(SimpleSlider.ForegroundProperty, bindingColor);
                slider.Foreground = new SolidColorBrush(param.Color.Value.ToColor());
                Binding binding = new Binding("Value");
                binding.Source = param.Offset;
                binding.Mode = BindingMode.TwoWay;
                slider.SetBinding(SimpleSlider.ValueProperty, binding);

                slider.DragStarted += (s, e) => { App.History.BeginGroup(); };
                slider.DragCompleted += (s, e) => { App.History.EndGroup(); };

                grid.Children.Add(slider);
            }

            return grid;
        }


        //
        public Brush CreateCanvasUIElement(int level)
        {
            if (Brush.Brush == null)
            {
                Brush.Brush = CreateBrush(); // ここがおかしい可能性あり
            }
            return Brush.Brush;
        }

        private Brush CreateBrush()
        { 
            if (Brush.Value == 1)
            {
                var brush = new SolidColorBrush();
                brush.Color = SolidColor.Value.ToColor();
                brush.Opacity = SolidColorOpacity.Value;
                return brush;
            }
            else if (Brush.Value == 2)
            {
                var brush = new LinearGradientBrush();
                brush.GradientStops = new GradientStopCollection();
                var p1 = new GradientStop(LinearGradientColor1.Value.ToColor(), LinearGradientOffset1.Value);
                brush.GradientStops.Add(p1);
                var p2 = new GradientStop(LinearGradientColor2.Value.ToColor(), LinearGradientOffset2.Value);
                brush.GradientStops.Add(p2);

                brush.StartPoint = new Point(LinearGradientStartPointX.Value, LinearGradientStartPointY.Value);
                brush.EndPoint = new Point(LinearGradientEndPointX.Value, LinearGradientEndPointY.Value);

                brush.Opacity = LinearGradientOpacity.Value;

                return brush;
            }
            else if (Brush.Value == 3)
            {
                var brush = new RadialGradientBrush();

                brush.GradientStops = new GradientStopCollection();
                var p1 = new GradientStop(RadialGradientColor1.Value.ToColor(), RadialGradientOffset1.Value);
                brush.GradientStops.Add(p1);
                var p2 = new GradientStop(RadialGradientColor2.Value.ToColor(), RadialGradientOffset2.Value);
                brush.GradientStops.Add(p2);

                brush.GradientOrigin = new Point(RadialGradientOriginX.Value, RadialGradientOriginY.Value);
                brush.Center = new Point(RadialGradientCenterX.Value, RadialGradientCenterY.Value);

                brush.RadiusX = RadialGradientRadiusX.Value;
                brush.RadiusY = RadialGradientRadiusY.Value;

                brush.Opacity = RadialGradientOpacity.Value;

                return brush;
            }
            else if (Brush.Value == 4)
            {
                var brush = new ImageBrush();
                //brush.Stretch = Stretch.Uniform;
                brush.ImageSource = ImagePath.Bitmap;
                brush.Opacity = ImageOpacity.Value;

                return brush;
            }
            else
            {
                return null;
            }
        }

        //
        public void UpdateBrush()
        {
            if (Brush.SampleBrush == null)
            {
                Brush.SampleBrush = CreateLinearGradientBrush();
            }
            SampleBrush = Brush.SampleBrush;
        }

        //
        private Brush CreateLinearGradientBrush()
        {
            if (Brush.Value == 1)
            {
                var brush = new SolidColorBrush();
                brush.Color = SolidColor.Value.ToColor();
                return brush;
            }
            else if (Brush.Value == 2)
            {
                var brush = new LinearGradientBrush();
                brush.GradientStops = new GradientStopCollection();
                var p1 = new GradientStop(LinearGradientColor1.Value.ToColor(), LinearGradientOffset1.Value);
                brush.GradientStops.Add(p1);
                var p2 = new GradientStop(LinearGradientColor2.Value.ToColor(), LinearGradientOffset2.Value);
                brush.GradientStops.Add(p2);

                brush.StartPoint = new Point(0, 0.5);
                brush.EndPoint = new Point(1, 0.5);

                return brush;
            }
            else if (Brush.Value == 3)
            {
                var brush = new LinearGradientBrush();
                brush.GradientStops = new GradientStopCollection();
                var p1 = new GradientStop(RadialGradientColor1.Value.ToColor(), RadialGradientOffset1.Value);
                brush.GradientStops.Add(p1);
                var p2 = new GradientStop(RadialGradientColor2.Value.ToColor(), RadialGradientOffset2.Value);
                brush.GradientStops.Add(p2);

                brush.StartPoint = new Point(0, 0.5);
                brush.EndPoint = new Point(1, 0.5);

                return brush;
            }
            else if (Brush.Value == 4)
            {
                var brush = new ImageBrush();
                //brush.Stretch = Stretch.Uniform;
                brush.ImageSource = ImagePath.Bitmap;

                return brush;
            }
            else
            {
                return null;
            }
        }
    }


    /// <summary>
    /// Parameter Brush
    /// </summary>
    [DataContract]
    public class LayerParameterBrush : LayerParameterInt
    {
        private LayerParameterBrush _Parent => (LayerParameterBrush)Parent;

        private Brush _Brush;
        public Brush Brush
        {
            get { return IsOverride || Parent == null ? _Brush : _Parent.Brush; }
            set { _Brush = value; }
        }

        private Brush _SampleBrush;
        public Brush SampleBrush
        {
            get { return IsOverride || Parent == null ? _SampleBrush : _Parent.SampleBrush; }
            set { _SampleBrush = value; }
        }
    }

}
