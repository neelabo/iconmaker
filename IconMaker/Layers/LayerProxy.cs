﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace IconMaker.Layers
{
    /// <summary>
    /// これダメ。要整備
    /// </summary>
    static public class IconSetting
    {
        static public int[] IconPixels = { 0, 256, 48, 32, 16 };
    }

    /// <summary>
    /// Layer Proxy Interface  ... いるのか？
    /// </summary>
    public interface ILayerProxy
    {
        void SetLevel(int id);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// 
    [DataContract]
    public class LayerProxy<T> : Layer, ILayerProxy where T : Layer, new()
    {
        public override string TypeName { get { return Default.TypeName; } }

        [DataMember]
        public T[] Layers { get; set; }

        //
        public T Default => Layers[0];

        //
        private T Current => Layers[Level];

        //
        public override Layer EditLayer { get { return Current; } }

        //
        public override CommonParameter Common
        {
            get { return Current.Common; }
            set { }
        }

        //
        public override global::System.Windows.Media.ImageSource Thumbnail
        {
            get { return Current.Thumbnail; }
            set { Current.Thumbnail = value; OnPropertyChanged(); }
        }

        // constructor
        public LayerProxy()
        {
            Layers = new T[IconSetting.IconPixels.Length];

            for (int i = 0; i < Layers.Length; ++i)
            {
                Layers[i] = new T();
            }

            //
            InitializeLayers();
        }

        //
        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            InitializeLayers();
        }


        //
        private void InitializeLayers()
        {
            int level = 0;
            foreach (var layer in Layers)
            {
                layer.Level = level++;
                layer.Link(Default);
                layer.PropertyChanged += ChildLayer_PropertyChanged;
                layer.UpdateThumbnail();
            }
        }


        // Relay NotifyPropertyChanged
        private void ChildLayer_PropertyChanged(object sender, global::System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        // set current edit level
        public void SetLevel(int level)
        {
            Level = level;
            Layers[Level].UpdateThumbnail(); // 各レベルサムネは表示時に更新されるようにする
            OnPropertyChanged("Common"); // リスト表示更新用
        }

        //
        public override void UpdateThumbnail()
        {
            foreach(var layer in Layers)
            {
                layer.UpdateThumbnail();
            }
        }

        //
        public override UIElement CreatePropertyEditerUIElement()
        {
            return Current?.CreatePropertyEditerUIElement();
        }

        //
        public override UIElement CreateCanvasUIElement(int level)
        {
            return Layers[level].CreateCanvasUIElement();
        }

        //
        public override UIElement CreateMaskedCanvasUIElement(int level)
        {
            return Layers[level].CreateMaskedCanvasUIElement(level);
        }

        //
        public override void Repare()
        {
            if (ID == 0)
            {
                ID = IconGenerator.Instance.UniqueID();
            }

            if (Label == null)
            {
                Label = IconGenerator.Instance.UniqueLabelNum("Layer 1");
            }

            foreach (var layer in Layers)
            {
                layer.Repare();
            }
        }
    }
}
