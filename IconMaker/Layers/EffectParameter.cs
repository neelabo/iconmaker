﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Effects;

namespace IconMaker.Layers
{
    [DataContract]
    public class EffectParameter
    {
        [DataMember]
        public LayerParameterInt Effect { get; set; }
        [DataMember]
        public LayerParameterDouble BlurEffectRadius { get; set; }
        [DataMember]
        public LayerParameterColor DropShadowEffectColor { get; set; }
        [DataMember]
        public LayerParameterDouble DropShadowEffectShadowDepth { get; set; }
        [DataMember]
        public LayerParameterDouble DropShadowEffectBlurRadius { get; set; }
        [DataMember]
        public LayerParameterDouble DropShadowEffectDirection { get; set; }
        [DataMember]
        public LayerParameterDouble DropShadowEffectOpacity { get; set; }

        //
        //Layer layer;

        //
        public EffectParameter()
        {
            Effect = new LayerParameterInt();
            BlurEffectRadius = new LayerParameterDouble() { Value = 5 };
            DropShadowEffectColor = new LayerParameterColor() { Value = "#FF000000" };
            DropShadowEffectShadowDepth = new LayerParameterDouble() { Value = 5 };
            DropShadowEffectBlurRadius = new LayerParameterDouble() { Value = 5 };
            DropShadowEffectDirection = new LayerParameterDouble() { Value = 315 };
            DropShadowEffectOpacity = new LayerParameterDouble() { Value = 1 };
        }

        public void InitializeParameterMap(string label, Layer layer)
        {
            //this.layer = layer;

            layer.ParameterMap[label + nameof(Effect)] = Effect;

            layer.ParameterMap[label + nameof(BlurEffectRadius)] = BlurEffectRadius;

            layer.ParameterMap[label + nameof(DropShadowEffectColor)] = DropShadowEffectColor;
            layer.ParameterMap[label + nameof(DropShadowEffectShadowDepth)] = DropShadowEffectShadowDepth;
            layer.ParameterMap[label + nameof(DropShadowEffectBlurRadius)] = DropShadowEffectBlurRadius;
            layer.ParameterMap[label + nameof(DropShadowEffectDirection)] = DropShadowEffectDirection;
            layer.ParameterMap[label + nameof(DropShadowEffectOpacity)] = DropShadowEffectOpacity;

            Effect.AddOverrideChild(BlurEffectRadius);
            Effect.AddOverrideChild(DropShadowEffectColor);
            Effect.AddOverrideChild(DropShadowEffectShadowDepth);
            Effect.AddOverrideChild(DropShadowEffectBlurRadius);
            Effect.AddOverrideChild(DropShadowEffectDirection);
            Effect.AddOverrideChild(DropShadowEffectOpacity);
        }


        public UIElement ParameterControl()
        {
            var items = new List<TabItem>();
            TabItem item;
            StackPanel stack;

            item = new TabItem();
            item.Header = "None";
            items.Add(item);

            item = new TabItem();
            item.Header = "Blur";
            stack = new StackPanel();
            stack.Children.Add(ParameterControls.Double("Radius", BlurEffectRadius));
            item.Content = stack;
            items.Add(item);

            item = new TabItem();
            item.Header = "DropShadow";
            stack = new StackPanel();
            stack.Children.Add(ParameterControls.Color("Color", DropShadowEffectColor));
            stack.Children.Add(ParameterControls.Double("ShadowDepth", DropShadowEffectShadowDepth));
            stack.Children.Add(ParameterControls.Double("BlurRadius", DropShadowEffectBlurRadius));
            stack.Children.Add(ParameterControls.Double("Direction", DropShadowEffectDirection));
            stack.Children.Add(ParameterControls.Percent("Opacity", DropShadowEffectOpacity));
            item.Content = stack;
            items.Add(item);

            //TabControl tabControl;
            return ParameterControls.Tab("Effect", Effect, items);
        }


        public Effect CreateCanvasUIElement(int level)
        {
            if (Effect.Value == 1)
            {
                var effect = new BlurEffect();
                effect.Radius = BlurEffectRadius.Value;
                effect.RenderingBias = RenderingBias.Quality;
                return effect;
            }
            else if (Effect.Value == 2)
            {
                var effect = new DropShadowEffect();
                effect.Color = DropShadowEffectColor.Value.ToColor();
                effect.ShadowDepth = DropShadowEffectShadowDepth.Value;
                effect.BlurRadius = DropShadowEffectBlurRadius.Value;
                effect.Direction = DropShadowEffectDirection.Value;
                effect.Opacity = DropShadowEffectOpacity.Value;
                effect.RenderingBias = RenderingBias.Quality;
                return effect;
            }
            else
            {
                return null;
            }
        }
    }
}
