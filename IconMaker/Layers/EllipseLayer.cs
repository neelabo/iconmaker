﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace IconMaker.Layers
{
    [DataContract]
    public class EllipseLayer : Layer
    {
        public override string TypeName { get { return "Ellipse"; } }

        [DataMember]
        public override CommonParameter Common { set; get; }

        [DataMember]
        public BrushParameter Brush { get; set; }

        [DataMember]
        public LayerParameterDouble StrokeThickness { set; get; }
        [DataMember]
        public BrushParameter StrokeBrush { set; get; }

        [DataMember]
        public EffectParameter Effect { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EllipseLayer()
        {
            Common = new CommonParameter();

            Brush = new BrushParameter();
            Brush.Brush.Value = 1;

            StrokeThickness = new LayerParameterDouble() { Value = 8 };
            StrokeBrush = new BrushParameter();

            Effect = new EffectParameter();


            InitializeParameterMap();
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            InitializeParameterMap();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeParameterMap()
        {
            ParameterMap = new Dictionary<string, LayerParameter>();

            Common.InitializeParameterMap(this);

            Brush.InitializeParameterMap(nameof(Brush), this);

            ParameterMap[nameof(StrokeThickness)] = StrokeThickness;
            StrokeBrush.InitializeParameterMap(nameof(StrokeBrush), this);

            Effect.InitializeParameterMap(nameof(Effect), this);

            base.InitializeParameterMapEvent();
        }


        public override UIElement CreatePropertyEditerUIElement()
        {
            var stackPanel = new global::System.Windows.Controls.StackPanel();

            stackPanel.Children.Add(Common.ParameterControl());

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(Brush.ParameterControl("FillBrush"));

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(StrokeBrush.ParameterControl("StrokeBrush"));
            stackPanel.Children.Add(ParameterControls.Double("StrokeThickness", StrokeThickness));

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(Effect.ParameterControl());

            return stackPanel;
        }


        ///
        public override void UpdateThumbnail()
        {
            // ブラシ更新
            Brush.UpdateBrush();
            StrokeBrush.UpdateBrush();

            base.UpdateThumbnail();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public override UIElement CreateCanvasUIElement(int level)
        {
            var ellipse = new Ellipse();

            Common.SetFrameworkElementParameter(ellipse, level);

            ellipse.Fill = Brush.CreateCanvasUIElement(level);

            ellipse.Stroke = StrokeBrush.CreateCanvasUIElement(level);
            ellipse.StrokeThickness = StrokeThickness.Value;

            ellipse.Effect = Effect.CreateCanvasUIElement(level);

            return ellipse;
        }
    }
}
