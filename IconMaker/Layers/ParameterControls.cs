﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace IconMaker.Layers
{
    static class ParameterControls
    {
        //
        public static Binding Binding(object source, string path)
        {
            var binding = new Binding(path);
            binding.Source = source;
            return binding;
        }

        //
        public static Binding Binding(object source, string path, BindingMode mode)
        {
            var binding = new Binding(path);
            binding.Source = source;
            binding.Mode = mode;
            return binding;
        }


        //
        public static DockPanel Header(string label, LayerParameter param)
        {
            var dockPanel = new DockPanel();
            DockPanel.SetDock(dockPanel, Dock.Top);
            dockPanel.Margin = new Thickness(2);

            dockPanel.Children.Add(HeaderTitle(label, param));

            return dockPanel;
        }

        public static DockPanel HeaderTitle(string label, LayerParameter param)
        {
            var dockPanel = new DockPanel();
            dockPanel.Margin = new Thickness(2, 0, 2, 0);
            dockPanel.Width = 100;

            if (!param.IsRoot && param.OverrideParent == null)
            {
                var checkBox = new CheckBox();
                checkBox.VerticalAlignment = VerticalAlignment.Center;
                checkBox.SetBinding(CheckBox.IsCheckedProperty, Binding(param, "IsOverride"));
                DockPanel.SetDock(checkBox, Dock.Left);

                dockPanel.Children.Add(checkBox);
            }

            var textBlock = new TextBlock();
            textBlock.Margin = new Thickness(2, 0, 0, 0);
            textBlock.Text = label;
            textBlock.ToolTip = label;
            textBlock.VerticalAlignment = VerticalAlignment.Center;
            textBlock.SetBinding(TextBlock.OpacityProperty, Binding(param, "Opacity"));
            dockPanel.Children.Add(textBlock);

            return dockPanel;
        }

        //
        public static DockPanel HeaderWarning(string label, LayerParameter param, Binding isWarning)
        {
            if (isWarning == null) return Header(label, param);

            var dockPanel = new DockPanel();
            DockPanel.SetDock(dockPanel, Dock.Top);
            dockPanel.Margin = new Thickness(2);

            var header = HeaderTitle(label, param);

            var image = new Image();
            image.Width = 15;
            image.Height = 15;
            image.HorizontalAlignment = HorizontalAlignment.Right;
            image.Margin = new Thickness(0, 0, 2, 0);
            image.Source = (DrawingImage)Application.Current.Resources["ic_warning_18px"];
            image.SetBinding(Image.VisibilityProperty, isWarning);
            image.SetBinding(TextBlock.OpacityProperty, Binding(param, "Opacity"));
            DockPanel.SetDock(image, Dock.Right);
            header.Children.Insert(0, image);

            dockPanel.Children.Add(header);

            return dockPanel;
        }

        //
        public static UIElement Bool(string label, LayerParameter param)
        {
            DockPanel dockPanel = Header(label, param);

            var checkBox = new CheckBox();
            checkBox.SetBinding(CheckBox.IsCheckedProperty, Binding(param, "Value"));
            checkBox.SetBinding(CheckBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(checkBox);

            return dockPanel;
        }

        //
        public static UIElement Double(string label, LayerParameter param)
        {
            DockPanel dockPanel = Header(label, param);

            var textBox = new TextBox();
            textBox.SetBinding(TextBox.TextProperty, Binding(param, "Value"));
            textBox.SetBinding(TextBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(textBox);

            return dockPanel;
        }
        

        //
        public static UIElement DoubleDouble(string label, LayerParameter param1, LayerParameter param2)
        {
            DockPanel dockPanel = Header(label, param1);

            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());

            var textBox = new TextBox();
            textBox.Margin = new Thickness(0, 0, 1, 0);
            textBox.SetBinding(TextBox.TextProperty, Binding(param1, "Value"));
            textBox.SetBinding(TextBox.IsEnabledProperty, Binding(param1, "IsEditable"));
            Grid.SetColumn(textBox, 0);

            grid.Children.Add(textBox);

            var textBox2 = new TextBox();
            textBox2.Margin = new Thickness(1, 0, 0, 0);
            textBox2.SetBinding(TextBox.TextProperty, Binding(param2, "Value"));
            textBox2.SetBinding(TextBox.IsEnabledProperty, Binding(param2, "IsEditable"));
            Grid.SetColumn(textBox2, 1);

            grid.Children.Add(textBox2);

            dockPanel.Children.Add(grid);

            return dockPanel;
        }


        //
        public static UIElement Percent(string label, LayerParameter param)
        {
            DockPanel dockPanel = Header(label, param);

            var textBox = new FormattedTextBox();
            Binding binding = Binding(param, "Value", BindingMode.TwoWay);
            binding.Converter = new PercentConverter();
            textBox.StringFormat = "{0}%";
            textBox.SetBinding(FormattedTextBox.TextProperty, binding);
            textBox.SetBinding(FormattedTextBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(textBox);

            return dockPanel;
        }


        [System.Windows.Data.ValueConversion(typeof(double), typeof(object))]
        private class PercentConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, global::System.Globalization.CultureInfo culture)
            {
                return ((double)value) * 100.0;
            }

            public object ConvertBack(object value, Type targetType, object parameter, global::System.Globalization.CultureInfo culture)
            {
                try
                {
                    var rate = double.Parse(((string)value).TrimEnd('%')) / 100.0;
                    if (rate < 0.0) rate = 0.0;
                    if (rate > 1.0) rate = 1.0;
                    return rate;
                }
                catch
                {
                    return value;
                }
            }
        }



        //
        public static UIElement ComboBox(string label, LayerParameter param, System.Collections.IEnumerable source)
        {
            DockPanel dockPanel = ParameterControls.Header(label, param);

            var comboBox = new ComboBox();

            comboBox.ItemsSource = source;
            comboBox.IsEditable = false;
            comboBox.SetBinding(System.Windows.Controls.ComboBox.SelectedValueProperty, ParameterControls.Binding(param, "Value"));
            comboBox.SetBinding(System.Windows.Controls.ComboBox.IsEnabledProperty, ParameterControls.Binding(param, "IsEditable"));

            dockPanel.Children.Add(comboBox);

            return dockPanel;
        }



        //
        public static UIElement Color(string label, LayerParameter param)
        {
            DockPanel dockPanel = Header(label, param);

            var textBox = new TextBox();
            textBox.Width = 80;
            textBox.BorderThickness = new Thickness(1, 1, 0, 1);
            textBox.Padding = new Thickness(0, 0, 2, 0);
            textBox.VerticalContentAlignment = VerticalAlignment.Center;
            Binding binding = Binding(param, "Value");
            binding.ValidatesOnExceptions = true;
            textBox.SetBinding(TextBox.TextProperty, binding);
            textBox.SetBinding(TextBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(textBox);

            var comboColorPicker = new OpenSourceControls.ComboColorPicker();
            comboColorPicker.SetBinding(OpenSourceControls.ComboColorPicker.SelectedColorProperty, Binding(param, "Value", BindingMode.TwoWay));
            comboColorPicker.SetBinding(OpenSourceControls.ComboColorPicker.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(comboColorPicker);

            return dockPanel;
        }


        //
        public static UIElement FontFamily(string label, LayerParameter param)
        {
            DockPanel dockPanel = Header(label, param);

            var comboBox = new ComboBox();
            var currentLang = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentCulture.IetfLanguageTag);
            var fonts = Fonts.SystemFontFamilies.Select(v => v.FamilyNames.FirstOrDefault(o => o.Key == currentLang).Value ?? v.Source);
            comboBox.ItemsSource = fonts;
            comboBox.IsEditable = false;
            comboBox.SetBinding(System.Windows.Controls.ComboBox.SelectedValueProperty, Binding(param, "Value"));
            comboBox.SetBinding(System.Windows.Controls.ComboBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(comboBox);

            return dockPanel;
        }

        //
        public static UIElement FontText(string label, LayerParameter param, LayerParameter paramFontFamily)
        {
            var isWarning = Binding(param, "Value");
            isWarning.Converter = new NullOrEmptyToVisiblityConverter();
            DockPanel dockPanel = HeaderWarning(label, param, isWarning);

            var textBox = new TextBox();
            textBox.MinHeight = 20;
            textBox.VerticalContentAlignment = VerticalAlignment.Center;
            textBox.AcceptsReturn = true;
            textBox.SetBinding(TextBox.TextProperty, Binding(param, "Value"));
            textBox.SetBinding(TextBox.FontFamilyProperty, Binding(paramFontFamily, "Value"));
            textBox.SetBinding(TextBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(textBox);

            return dockPanel;
        }


        [System.Windows.Data.ValueConversion(typeof(string), typeof(Visibility))]
        public class NullOrEmptyToVisiblityConverter : System.Windows.Data.IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (string.IsNullOrEmpty((string)value))
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Hidden;
                }
            }

            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }



        //
        public static UIElement Tab(string label, LayerParameter param, List<TabItem> items)
        {
            var dockPanel = new DockPanel();
            DockPanel.SetDock(dockPanel, Dock.Top);

            var dockPanelHead = Header(label, param);
            dockPanelHead.Children.Add(new UIElement());
            dockPanel.Children.Add(dockPanelHead);

            var tabControl = new TabControl();
            tabControl.Margin = new Thickness(2);
            tabControl.SetBinding(TabControl.SelectedIndexProperty, Binding(param, "Value"));
            tabControl.SetBinding(TabControl.IsEnabledProperty, Binding(param, "IsEditable"));
            foreach (var item in items)
            {
                tabControl.Items.Add(item);
            }

            dockPanel.Children.Add(tabControl);

            return dockPanel;
        }

        //
        public static UIElement FileName(string label, LayerParameterPath param, Microsoft.Win32.OpenFileDialog openFileDialog=null)
        {
            var isWarning = Binding(param, "IsValid");
            isWarning.Converter = new NotBoolToVisiblityConverter();
            DockPanel dockPanel = HeaderWarning(label, param, isWarning);

            var filenameBox = new FilenameBox();
            if (openFileDialog != null) filenameBox.OpenFileDialog = openFileDialog;
            filenameBox.SetBinding(FilenameBox.TextProperty, Binding(param, "Value", BindingMode.TwoWay));
            filenameBox.SetBinding(FilenameBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(filenameBox);

            return dockPanel;
        }

        //
        public static UIElement Layer(string label, LayerParameter param)
        {
            DockPanel dockPanel = Header(label, param);

            var comboBox = new ComboBox();

            var items = IconGenerator.Instance.Layers.Where(e => e != IconGenerator.Instance.SelectedLayer).ToList();
            items.Insert(0, new NullLayer());

            comboBox.ItemsSource = items;
            comboBox.IsEditable = false;
            comboBox.SetBinding(System.Windows.Controls.ComboBox.SelectedValueProperty, Binding(param, "Value"));
            comboBox.SetBinding(System.Windows.Controls.ComboBox.IsEnabledProperty, Binding(param, "IsEditable"));

            dockPanel.Children.Add(comboBox);

            return dockPanel;
        }

    }
}
