﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IconMaker.Layers
{
    [DataContract]
    public class CommonParameter
    {
        [DataMember]
        public LayerParameterBool IsVisible { set; get; }

        [DataMember]
        public LayerParameterDouble Left { set; get; }
        [DataMember]
        public LayerParameterDouble Top { set; get; }
        [DataMember]
        public LayerParameterDouble Width { set; get; }
        [DataMember]
        public LayerParameterDouble Height { set; get; }
        [DataMember]
        public LayerParameterDouble Angle { set; get; }

        [DataMember(Order = 1)]
        public LayerParameterLayer Mask { set; get; }

        //
        Layer _Layer;

        //
        public CommonParameter()
        {
            IsVisible = new LayerParameterBool() { Value = true };

            Left = new LayerParameterDouble();
            Top = new LayerParameterDouble();
            Width = new LayerParameterDouble() { Value = 256 };
            Height = new LayerParameterDouble() { Value = 256 };
            Angle = new LayerParameterDouble();

            Constructor();
        }

        //
        [OnDeserializing]
        private void Deserializing(StreamingContext c)
        {
            Constructor();
        }

        private void Constructor()
        {
            Mask = new LayerParameterLayer();
        }



        //
        public void InitializeParameterMap(Layer layer)
        {
            string label = "Common";

            this._Layer = layer;

            layer.ParameterMap[label + nameof(IsVisible)] = IsVisible;
            layer.ParameterMap[label + nameof(Left)] = Left;
            layer.ParameterMap[label + nameof(Top)] = Top;
            layer.ParameterMap[label + nameof(Width)] = Width;
            layer.ParameterMap[label + nameof(Height)] = Height;
            layer.ParameterMap[label + nameof(Angle)] = Angle;

            layer.ParameterMap[label + nameof(Mask)] = Mask;
        }

        //
        public UIElement ParameterControl()
        {
            var stackPanel = new global::System.Windows.Controls.StackPanel();

            stackPanel.Children.Add(ParameterControls.Bool("Visible", IsVisible));
            stackPanel.Children.Add(ParameterControls.Double("Left", Left));
            stackPanel.Children.Add(ParameterControls.Double("Top", Top));
            stackPanel.Children.Add(ParameterControls.Double("Width", Width));
            stackPanel.Children.Add(ParameterControls.Double("Height", Height));
            stackPanel.Children.Add(ParameterControls.Double("Angle", Angle));

            stackPanel.Children.Add(ParameterControls.Layer("Mask", Mask));

            return stackPanel;
        }

        //
        public void SetFrameworkElementParameter(FrameworkElement element, int level)
        {
            element.Visibility = IsVisible.Value ? Visibility.Visible : Visibility.Hidden;

            Canvas.SetTop(element, Top.Value);
            Canvas.SetLeft(element, Left.Value);
            element.Width = Width.Value;
            element.Height = Height.Value;

            RotateTransform rotateTransform1 = new RotateTransform(Angle.Value, element.Width * 0.5, element.Height * 0.5);
            element.RenderTransform = rotateTransform1;
        }

        public UIElement MaskedElement(UIElement element, int level)
        {
            if (Mask.Value == null) return element;

            var maskCanvas = new Canvas();
            maskCanvas.Width = 256;
            maskCanvas.Height = 256;
            maskCanvas.Background = Brushes.Transparent;
            var maskElement = Mask.Value.CreateCanvasUIElement(level);
            maskElement.Visibility = Visibility.Visible; // マスクは常に表示
            maskCanvas.Children.Add(maskElement);
            maskCanvas.Clip = new RectangleGeometry(new Rect(0, 0, 256, 256)); // Viewportサイズ確定
            var brush = new VisualBrush(maskCanvas);
            brush.Stretch = Stretch.None;
            brush.Viewport = new Rect(0, 0, 256, 256);
            brush.ViewportUnits = BrushMappingMode.Absolute;

            var mainCanvas = new Canvas(); // マスクと座標をあわせるため、キャンバス化
            mainCanvas.Children.Add(element);
            mainCanvas.OpacityMask = brush;

            return mainCanvas;
        }

        //
        public void Repare()
        {
            Mask.Repare();
        }
    }



    /// <summary>
    /// ParameterLayer
    /// </summary>
    [DataContract]
    public class LayerParameterLayer : LayerParameter
    {
        private LayerParameterLayer _Parent => (LayerParameterLayer)Parent;

        //
        private Layer _Value;
        public Layer Value
        {
            get { return IsOverride || Parent == null ? _Value : _Parent.Value; }
            set
            {
                var val = (value is NullLayer) ? null : value;
                if (_Value != val)
                {
                    App.History.Add(new MementoParameterLayer(this, _Value, value));
                    _Value = val;
                    OnValueChanged();
                }
            }
        }

        // レイヤーのシリアル化用（IDで保存）
        private int _LayerID;
        [DataMember]
        public int LayerID
        {
            get { return _Value == null ? 0 : _Value.ID; }
            set { _LayerID = value; }
        }

        //
        public void Repare()
        {
            if (_Value != null)
            {
                if (!IconGenerator.Instance.Layers.Any(e => e == _Value))
                {
                    Value = null;
                }
            }
            else if (_LayerID != 0)
            {
                Value = IconGenerator.Instance.Layers.FirstOrDefault(e => e.ID == _LayerID);
            }
            _LayerID = 0;
        }

        //
        protected override void CopyValueFromParent()
        {
            if (Parent != null)
            {
                Value = _Parent.Value;
            }
        }

        //
        protected virtual void OnValueChanged()
        {
            OnPropertyChanged(nameof(Value));
        }
    }


    /// <summary>
    /// ParameterLayer の MementoCommand
    /// </summary>
    public class MementoParameterLayer : MementoCommand<LayerParameterLayer, Layer>
    {
        public MementoParameterLayer(LayerParameterLayer target, Layer v0, Layer v1) : base(target, v0, v1)
        {
        }

        public override void Redo()
        {
            Target.Value = Value1;
        }

        public override void Undo()
        {
            Target.Value = Value0;
        }

        public override bool EqualsTarget(IMementoCommand target)
        {
            return (this.GetType() == target.GetType() && this.Target == ((MementoParameterLayer)target).Target);
        }

        public override void Merge(IMementoCommand target)
        {
            Value0 = ((MementoParameterLayer)target).Value0;
        }
    }


}
