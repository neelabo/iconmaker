﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace IconMaker.Layers
{
    [DataContract]
    public abstract class LayerParameter : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([global::System.Runtime.CompilerServices.CallerMemberNameAttribute] string name = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
            }
        }
        #endregion

        // parent
        public LayerParameter Parent { get; set; }

        //
        [DataMember]
        private bool _IsOverride;
        public bool IsOverride
        {
            get
            {
                return _IsOverride;
            }
            set
            {
                if (_IsOverride != value)
                {
                    App.History.BeginGroup();

                    CopyValueFromParent();

                    App.History.Add(new MementoParameterOverride(this, _IsOverride, value));
                    _IsOverride = value;

                    OnPropertyChanged();
                    OnPropertyChanged("IsEditable");
                    OnPropertyChanged("Opacity");

                    foreach (var child in OverrideChild)
                    {
                        child.IsOverride = value;
                    }

                    App.History.EndGroup();
                }
            }
        }

        //
        public List<LayerParameter> OverrideChild { get; private set; }
        public LayerParameter OverrideParent { get; private set; }

        //
        public bool IsRoot => Parent == null;

        //
        public bool IsEditable => IsOverride || Parent == null;

        //
        public double Opacity => IsEditable ? 1.0 : 0.5;

        //
        protected abstract void CopyValueFromParent();

        //
        public LayerParameter()
        {
            Constructor();
        }

        [OnDeserializing]
        private void Deserializing(StreamingContext c)
        {
            Constructor();
        }

        private void Constructor()
        {
            OverrideChild = new List<LayerParameter>();
        }

        //
        public void AddOverrideChild(LayerParameter child)
        {
            OverrideChild.Add(child);
            child.OverrideParent = this;
        }
    }


    // generic parameter
    [DataContract]
    public abstract class LayerParameter<T> : LayerParameter
    {
        private LayerParameter<T> _Parent => (LayerParameter<T>)Parent;

        //
        [DataMember]
        private T _Value;
        public T Value
        {
            get { return IsOverride || Parent == null ? _Value : _Parent.Value; }
            set
            {
                if (IsEqual(_Value, value)) return;
                if (!Verify(value)) throw new InvalidCastException();
                App.History.Add(new MementoParameter<T>(this, _Value, value));
                SetPrivateValue(value);
            }
        }

        //
        protected override void CopyValueFromParent()
        {
            if (Parent != null)
            {
                Value = _Parent.Value;
            }
        }

        //
        protected abstract bool IsEqual(T v0, T v1);

        //
        protected virtual void SetPrivateValue(T value)
        {
            _Value = value;
            OnPropertyChanged(nameof(Value));
        }

        //
        protected virtual bool Verify(T value)
        {
            return true;
        }
    }


    [DataContract]
    public class LayerParameterBool : LayerParameter<bool>
    {
        protected override bool IsEqual(bool v0, bool v1)
        {
            return v0 == v1;
        }
    }

    [DataContract]
    public class LayerParameterInt : LayerParameter<int>
    {
        protected override bool IsEqual(int v0, int v1)
        {
            return v0 == v1;
        }
    }

    [DataContract]
    public class LayerParameterDouble : LayerParameter<double>
    {
        protected override bool IsEqual(double v0, double v1)
        {
            return v0 == v1;
        }
    }

    [DataContract]
    public class LayerParameterString : LayerParameter<string>
    {
        protected override bool IsEqual(string v0, string v1)
        {
            return v0 == v1;
        }
    }

    [DataContract]
    public class LayerParameterColor : LayerParameterString
    {
        protected override bool Verify(string value)
        {
            return System.Windows.Media.ColorConverter.ConvertFromString(value) is System.Windows.Media.Color;
        }
    }

    [DataContract]
    public class LayerParameterPath : LayerParameterString
    {
        private LayerParameterPath _Parent => (LayerParameterPath)Parent;

        private bool _IsValid;
        public bool IsValid
        {
            get { return IsOverride || Parent == null ? _IsValid : _Parent._IsValid; }
            set { _IsValid = value; OnPropertyChanged(); }
        }
    }





    class MementoParameterOverride : MementoCommand<LayerParameter, bool>
    {
        public MementoParameterOverride(LayerParameter target, bool v0, bool v1) : base(target, v0, v1) { }

        public override void Redo()
        {
            Target.IsOverride = Value1;
        }

        public override void Undo()
        {
            Target.IsOverride = Value0;
        }

        public override bool EqualsTarget(IMementoCommand target)
        {
            return (this.GetType() == target.GetType() && this.Target == ((MementoParameterOverride)target).Target);
        }

        public override void Merge(IMementoCommand target)
        {
            Value0 = ((MementoParameterOverride)target).Value0;
        }
    }


    class MementoParameter<T> : MementoCommand<LayerParameter<T>, T>
    {
        public MementoParameter(LayerParameter<T> target, T v0, T v1) : base(target, v0, v1) { }

        public override void Redo()
        {
            Target.Value = Value1;
        }

        public override void Undo()
        {
            Target.Value = Value0;
        }

        public override bool EqualsTarget(IMementoCommand target)
        {
            return (this.GetType() == target.GetType() && this.Target == ((MementoParameter<T>)target).Target);
        }

        public override void Merge(IMementoCommand target)
        {
            Value0 = ((MementoParameter<T>)target).Value0;
        }
    }
}
