﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IconMaker.Layers
{
    public static class ColorExtension
    {
        /// <summary>
        /// convert to color struct. 
        /// </summary>
        /// <param name="value">color string (ex. #FFAA00CC)</param>
        /// <returns>Color</returns>
        public static global::System.Windows.Media.Color ToColor(this string value)
        {
            try
            {
                System.Windows.Media.Color? color = System.Windows.Media.ColorConverter.ConvertFromString(value) as System.Windows.Media.Color?;
                if (color != null)
                    return (System.Windows.Media.Color)color;
                else
                    return new System.Windows.Media.Color();
            }
            catch
            {
                return new System.Windows.Media.Color();
            }
        }
    }

}
