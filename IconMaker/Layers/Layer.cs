﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Data;
using System.Runtime.Serialization;
using System.Globalization;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace IconMaker.Layers
{
    // イベント処理用のデリゲート
    public delegate void CommandEventHandler(Layer sender, string command);


    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public abstract class Layer : INotifyPropertyChanged
    {

        #region NotifyPropertyChanged
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([global::System.Runtime.CompilerServices.CallerMemberNameAttribute] string name = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
            }
        }
        #endregion

        // common parameters
        [DataMember]
        public abstract CommonParameter Common { set;  get; }

        // serial no.
        [DataMember(Order = 1)]
        public int ID { get; set; }

        // layer label.
        [DataMember(Order = 1)]
        public string _Label;
        public string Label
        {
            get { return _Label; }
            set { if (_Label != value) { App.History.Add(new MementoLabel(this, _Label, value)); _Label = value; OnPropertyChanged(); } }
        }

        public abstract string TypeName { get; }

        public override string ToString()
        {
            return (Label != null) ? Label : TypeName;
        }

        // layer level.
        public int Level { get; set; }

        // make image UI element.
        public virtual UIElement CreateCanvasUIElement(int level = 0) { return null; }

        // make masked UI element.
        public virtual UIElement CreateMaskedCanvasUIElement(int level = 0)
        {
            return Common.MaskedElement(CreateCanvasUIElement(level), level);
        }

        // make editable property UI element.
        public virtual UIElement CreatePropertyEditerUIElement() { return null; }

        // layer thumbnail.
        private ImageSource _Thumbnail;
        public virtual ImageSource Thumbnail
        {
            get { return _Thumbnail; }
            set { _Thumbnail = value; OnPropertyChanged(); }
        }

        // parameter map.
        public Dictionary<string, LayerParameter> ParameterMap { get; set; }


        // main constructor
        private void Constructor()
        {
        }

        // constructor
        public Layer()
        {
            Constructor();
        }

        [OnDeserializing]
        private void Deserializing(StreamingContext c)
        {
            Constructor();
        }


        // initialize parameter map event setting.
        public void InitializeParameterMapEvent()
        {
            foreach (var param in ParameterMap)
            {
                param.Value.PropertyChanged += Param_PropertyChanged;
            }
        }

        // これなー
        int _RequestCount;

        // if change parameter, then update thumbnail image.
        private async void Param_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_RequestCount++ == 0)
            {
                // 同時に何度もリクエストが来た時にまとめて更新するように遅延させる
                await Task.Run(async () => { await Task.Delay(10); });
                //System.Diagnostics.Debug.WriteLine($"REQ={_RequestCount}");
                _RequestCount = 0;
                UpdateThumbnail();
            }
        }


        // link to parent layer.
        public void Link(Layer parent)
        {
            if (parent == this) return;

            foreach (var param in ParameterMap)
            {
                param.Value.Parent = parent?.ParameterMap[param.Key];
            }
        }

        // update thumbnail image.
        public virtual void UpdateThumbnail()
        {
            int pixel = 48;
            double dpi = 96.0 * pixel / 256.0;

            var canvas = new Canvas();
            canvas.Width = pixel;
            canvas.Height = pixel;

            var element = CreateCanvasUIElement();
            element.Visibility = Visibility.Visible; // サムネイルは強制表示

            canvas.Children.Add(element);

            canvas.Measure(new Size(canvas.Width, canvas.Height));
            canvas.Arrange(new Rect(new Size(canvas.Width, canvas.Height)));
            canvas.UpdateLayout();

            RenderTargetBitmap bmp = new RenderTargetBitmap(pixel, pixel, dpi, dpi, PixelFormats.Pbgra32);
            bmp.Render(canvas);

            Thumbnail = bmp;
        }

        // clone.
        public Layer Clone()
        {
            Type[] LayerTypes = new Type[] {
                typeof(LayerProxy<TextLayer>),
                typeof(LayerProxy<RectangleLayer>),
                typeof(LayerProxy<EllipseLayer>),
                typeof(LayerProxy<BitmapLayer>),
                typeof(LayerProxy<SvgLayer>),
            };

            using (var ms = new global::System.IO.MemoryStream())
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Layer), LayerTypes);
                serializer.WriteObject((global::System.IO.Stream)ms, (System.Object)(this));
                ms.Position = 0;
                Layer layer = (Layer)serializer.ReadObject(ms);
                layer.Repare();

                return layer;
            }
        }

        //
        public virtual Layer EditLayer { get { return this; } }

        //
        public virtual void Repare()
        {
            Common.Repare();
        }
    }


    /// <summary>
    /// Label MementoCommand
    /// </summary>
    public class MementoLabel : MementoCommand<Layer, string>
    {
        public MementoLabel(Layer target, string v0, string v1) : base(target, v0, v1)
        {
        }

        public override void Redo()
        {
            Target.Label = Value1;
        }

        public override void Undo()
        {
            Target.Label = Value0;
        }

        public override bool EqualsTarget(IMementoCommand target)
        {
            return (this.GetType() == target.GetType() && this.Target == ((MementoLabel)target).Target);
        }

        public override void Merge(IMementoCommand target)
        {
            Value0 = ((MementoLabel)target).Value0;
        }
    }


}
