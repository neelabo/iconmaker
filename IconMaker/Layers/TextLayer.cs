﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.Serialization;
using System.Windows.Media.Effects;

namespace IconMaker.Layers
{
    /// <summary>
    /// Text Layer
    /// </summary>
    [DataContract]
    public class TextLayer : Layer
    {
        static Dictionary<string, FontWeight> FontWeightTable = new Dictionary<string, FontWeight>
        {
            ["Black"] = FontWeights.Black,
            ["Bold"] = FontWeights.Bold,
            ["DemiBold"] = FontWeights.DemiBold,
            ["ExtraBlack"] = FontWeights.ExtraBlack,
            ["ExtraBold"] = FontWeights.ExtraBold,
            ["ExtraLight"] = FontWeights.ExtraLight,
            ["Heavy"] = FontWeights.Heavy,
            ["Light"] = FontWeights.Light,
            ["Medium"] = FontWeights.Medium,
            ["Normal"] = FontWeights.Normal,
            ["Regular"] = FontWeights.Regular,
            ["SemiBold"] = FontWeights.SemiBold,
            ["Thin"] = FontWeights.Thin,
            ["UltraBlack"] = FontWeights.UltraBlack,
            ["UltraBold"] = FontWeights.UltraBold,
            ["UltraLight"] = FontWeights.UltraLight,
        };

        static Dictionary<string, FontStyle> FontStyleTable = new Dictionary<string, FontStyle>
        {
            ["Italic"] = FontStyles.Italic,
            ["Normal"] = FontStyles.Normal,
            ["Oblique"] = FontStyles.Oblique,
        };

        static Dictionary<string, FontStretch> FontStretchTable = new Dictionary<string, FontStretch>
        {
            ["Condensed"] = FontStretches.Condensed,
            ["Expanded"] = FontStretches.Expanded,
            ["ExtraCondensed"] = FontStretches.ExtraCondensed,
            ["ExtraExpanded"] = FontStretches.ExtraExpanded,
            ["Medium"] = FontStretches.Medium,
            ["Normal"] = FontStretches.Normal,
            ["SemiCondensed"] = FontStretches.SemiCondensed,
            ["SemiExpanded"] = FontStretches.SemiExpanded,
            ["UltraCondensed"] = FontStretches.UltraCondensed,
            ["UltraExpanded"] = FontStretches.UltraExpanded,
        };


        public override string TypeName { get { return "Text"; } }

        [DataMember]
        public override CommonParameter Common { set; get; }

        [DataMember]
        public BrushParameter Brush { get; set; }

        [DataMember]
        public LayerParameterString Text { get; set; }
        [DataMember]
        public LayerParameterString FontFamily { get; set; }
        [DataMember]
        public LayerParameterDouble FontSize { get; set; }
        [DataMember]
        public LayerParameterString FontWeight { get; set; }
        [DataMember]
        public LayerParameterString FontStyle { get; set; }
        //[DataMember]
        //public LayerParameterString FontStretch { get; set; } // 効果があるフォントが殆ど無いので無効にした
        [DataMember(Order = 1)]
        public LayerParameterHorizontalAlignment HorizontalAlignment { get; set; }
        [DataMember(Order = 1)]
        public LayerParameterVerticalAlignment VerticalAlignment { get; set; }
        [DataMember(Order = 1)]
        public LayerParameterTextAlignment TextAlignment { get; set; }

        [DataMember]
        public EffectParameter Effect { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public TextLayer()
        {
            Constructor();

            InitializeParameterMap();
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext c)
        {
            Constructor();
        }

        private void Constructor()
        {
            Common = new CommonParameter();

            Brush = new BrushParameter();
            Brush.Brush.Value = 1;
            Brush.SolidColor.Value = "#FFFFFFFF";

            Text = new LayerParameterString();
            FontFamily = new LayerParameterString() { Value = "Arial" };
            FontSize = new LayerParameterDouble() { Value = 100 };
            FontWeight = new LayerParameterString() { Value = "Normal" };
            FontStyle = new LayerParameterString() { Value = "Normal" };
            //FontStretch = new LayerParameterString() { Value = "Normal" };
            HorizontalAlignment = new LayerParameterHorizontalAlignment() { Value = System.Windows.HorizontalAlignment.Stretch };
            VerticalAlignment = new LayerParameterVerticalAlignment() { Value = System.Windows.VerticalAlignment.Stretch };
            TextAlignment = new LayerParameterTextAlignment() { Value = System.Windows.TextAlignment.Left };

            Effect = new EffectParameter();
        }


        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            InitializeParameterMap();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeParameterMap()
        {
            ParameterMap = new Dictionary<string, LayerParameter>();

            Common.InitializeParameterMap(this);

            Brush.InitializeParameterMap(nameof(Brush), this);

            ParameterMap[nameof(Text)] = Text;
            ParameterMap[nameof(FontFamily)] = FontFamily;
            ParameterMap[nameof(FontSize)] = FontSize;
            ParameterMap[nameof(FontWeight)] = FontWeight;
            ParameterMap[nameof(FontStyle)] = FontStyle;
            //ParameterMap[nameof(FontStretch)] = FontStretch;
            ParameterMap[nameof(HorizontalAlignment)] = HorizontalAlignment;
            ParameterMap[nameof(VerticalAlignment)] = VerticalAlignment;
            ParameterMap[nameof(TextAlignment)] = TextAlignment;

            Effect.InitializeParameterMap(nameof(Effect), this);

            base.InitializeParameterMapEvent();
        }


        //
        public override UIElement CreatePropertyEditerUIElement()
        {
            var stackPanel = new global::System.Windows.Controls.StackPanel();

            stackPanel.Children.Add(Common.ParameterControl());

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(Brush.ParameterControl("Brush"));

            stackPanel.Children.Add(ParameterControls.FontText("Text", Text, FontFamily));
            stackPanel.Children.Add(ParameterControls.FontFamily("FontFamily", FontFamily));
            stackPanel.Children.Add(ParameterControls.Double("FontSize", FontSize));
            stackPanel.Children.Add(ParameterControls.ComboBox("FontWeight", FontWeight, FontWeightTable.Select(v => v.Key)));
            stackPanel.Children.Add(ParameterControls.ComboBox("FontStyle", FontStyle, FontStyleTable.Select(v => v.Key)));
            //stackPanel.Children.Add(ParameterControls.ParameterComboBox("FontStretch", FontStretch, FontStretchTable.Select(v => v.Key)));
            stackPanel.Children.Add(ParameterControls.ComboBox("HorizontalAlignment", HorizontalAlignment, Enum.GetValues(typeof(HorizontalAlignment))));
            stackPanel.Children.Add(ParameterControls.ComboBox("VerticalAlignment", VerticalAlignment, Enum.GetValues(typeof(VerticalAlignment))));
            stackPanel.Children.Add(ParameterControls.ComboBox("TextAlignment", TextAlignment, Enum.GetValues(typeof(TextAlignment))));

            stackPanel.Children.Add(new Separator());
            stackPanel.Children.Add(Effect.ParameterControl());

            return stackPanel;
        }



        //
        public override void UpdateThumbnail()
        {
            // ブラシ更新
            Brush.UpdateBrush();

            base.UpdateThumbnail();
        }


        //
        public override UIElement CreateCanvasUIElement(int level)
        {
            if (FontSize.Value <= 0) return new UIElement(); // dummy

            var grid = new Grid();
            Common.SetFrameworkElementParameter(grid, level);
            
            var textBlock = new TextBlock();
            if (FontFamily.Value != null) textBlock.FontFamily = new FontFamily(FontFamily.Value);
            textBlock.Text = Text.Value;
            textBlock.FontSize = FontSize.Value;
            textBlock.HorizontalAlignment = HorizontalAlignment.Value;
            textBlock.VerticalAlignment = VerticalAlignment.Value;
            if (FontWeightTable.ContainsKey(FontWeight.Value))
            {
                textBlock.FontWeight = FontWeightTable[FontWeight.Value];
            }
            if (FontWeightTable.ContainsKey(FontStyle.Value))
            {
                textBlock.FontStyle = FontStyleTable[FontStyle.Value];
            }
            //if (!string.IsNullOrEmpty(FontStretch.Value))
            //{
            //    textBlock.FontStretch = FontStretchTable[FontStretch.Value];
            //}
            textBlock.Foreground = Brush.CreateCanvasUIElement(level);
            textBlock.TextAlignment = TextAlignment.Value;
            textBlock.Effect = Effect.CreateCanvasUIElement(level);
            grid.Children.Add(textBlock);

            return grid;
        }

    }


    public class LayerParameterTextAlignment : LayerParameter<TextAlignment>
    {
        protected override bool IsEqual(TextAlignment v0, TextAlignment v1)
        {
            return v0 == v1;
        }
    }

    public class LayerParameterHorizontalAlignment : LayerParameter<HorizontalAlignment>
    {
        protected override bool IsEqual(HorizontalAlignment v0, HorizontalAlignment v1)
        {
            return v0 == v1;
        }
    }

    public class LayerParameterVerticalAlignment : LayerParameter<VerticalAlignment>
    {
        protected override bool IsEqual(VerticalAlignment v0, VerticalAlignment v1)
        {
            return v0 == v1;
        }
    }
}
