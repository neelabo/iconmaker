﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using Svg2Xaml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IconMaker.Layers
{

    [DataContract]
    public class SvgLayer : Layer 
    {
        public override string TypeName { get { return "SVG"; } }

        [DataMember]
        public override CommonParameter Common { get; set; }

        [DataMember]
        public LayerParameterSvg Path { get; set; }
        [DataMember]
        public LayerParameterDouble Opacity { get; set; }
        [DataMember(Order=1)]
        public BrushParameter Brush { get; set; }

        [DataMember]
        public EffectParameter Effect { get; set; }

        //
        public SvgLayer()
        {
            Constructor();

            InitializeParameterMap();
        }

        [OnDeserializing]
        private void Constructor(StreamingContext c)
        {
            Constructor();
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            InitializeParameterMap();
        }

        private void Constructor()
        { 
            Common = new CommonParameter();

            Path = new LayerParameterSvg();
            Opacity = new LayerParameterDouble() { Value = 1.0 };
            Brush = new BrushParameter();

            Effect = new EffectParameter();
        }

        private void InitializeParameterMap()
        {
            ParameterMap = new Dictionary<string, LayerParameter>();

            Common.InitializeParameterMap(this);

            ParameterMap[nameof(Path)] = Path;
            ParameterMap[nameof(Opacity)] = Opacity;
            Brush.InitializeParameterMap(nameof(Brush), this);

            Effect.InitializeParameterMap(nameof(Effect), this);

            base.InitializeParameterMapEvent();
        }

        public override UIElement CreatePropertyEditerUIElement()
        {
            var stackPanel = new global::System.Windows.Controls.StackPanel();

            stackPanel.Children.Add(Common.ParameterControl());

            stackPanel.Children.Add(new Separator());

            stackPanel.Children.Add(ParameterControls.FileName("SVG", Path, OpenFileDialogTools.CreateOpenSvgFileDialog()));
            stackPanel.Children.Add(ParameterControls.Percent("Opacity", Opacity));
            stackPanel.Children.Add(Brush.ParameterControl("Brush"));

            stackPanel.Children.Add(new Separator());

            stackPanel.Children.Add(Effect.ParameterControl());

            return stackPanel;
        }


        ///
        public override void UpdateThumbnail()
        {
            // ブラシ更新
            Brush.UpdateBrush();
            //StrokeBrush.UpdateBrush();

            base.UpdateThumbnail();
        }

        public override UIElement CreateCanvasUIElement(int level)
        {
            var image = new Image();

            Common.SetFrameworkElementParameter(image, level);

            image.Source = Path.DrawingImage;

            if (Path.DrawingImage != null)
            {
                Path.SetBrush(Brush.CreateCanvasUIElement(level));
            }

            image.Opacity = Opacity.Value;

            image.Effect = Effect.CreateCanvasUIElement(level);

            return image;
        }
    }

    /// <summary>
    /// LayerParameter SVG
    /// </summary>
    [DataContract]
    public class LayerParameterSvg : LayerParameterPath
    {
        private LayerParameterSvg _Parent => (LayerParameterSvg)Parent;

        private DrawingImage _DrawingImage;
        public DrawingImage DrawingImage
        {
            get { return IsOverride || Parent == null ? _DrawingImage : _Parent.DrawingImage; }
            set { _DrawingImage = value; }
        }

        public struct BrushSet
        {
            public Brush FillBrush;
            public Brush PenBrush;
        }

        private Dictionary<GeometryDrawing, BrushSet> _OriginalBrushes;
        public Dictionary<GeometryDrawing, BrushSet> OriginalBrushes
        {
            get { return IsOverride || Parent == null ? _OriginalBrushes : _Parent.OriginalBrushes; }
            set { _OriginalBrushes = value; }
        }

        [OnDeserialized]
        private void Deserializing(StreamingContext c)
        {
            Load(Value);
        }

        //
        protected override void SetPrivateValue(string value)
        {
            Load(value);
            base.SetPrivateValue(value);
        }

        private void Load(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                DrawingImage = null;
                OriginalBrushes = null;
                IsValid = false;
                return;
            }

            try
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    DrawingImage = SvgReader.Load(stream);
                }
                StoreBrush();
                IsValid = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                DrawingImage = null;
                OriginalBrushes = null;
                IsValid = false;
            }
        }

        // トレースし、すべてのブラシを保存
        private void StoreBrush()
        {
            OriginalBrushes = new Dictionary<GeometryDrawing, BrushSet>();

            foreach (var geometry in AllGeometry(DrawingImage.Drawing))
            {
                OriginalBrushes.Add(geometry, new BrushSet() { FillBrush = geometry.Brush, PenBrush = geometry.Pen?.Brush });
            }
        }

        // ブラシ設定の置き換え
        public void SetBrush(Brush brush)
        {
            foreach (var pair in OriginalBrushes)
            {
                pair.Key.Brush = brush ?? pair.Value.FillBrush;
                if (pair.Key.Pen != null)
                    pair.Key.Pen.Brush = brush ?? pair.Value.PenBrush;
            }
        }

        // すべてのGeometryDrawingをトレースする
        private IEnumerable<GeometryDrawing> AllGeometry(Drawing drawing)
        {
            if (drawing is GeometryDrawing)
            {
                yield return (GeometryDrawing)drawing;
            }
            else if (drawing is DrawingGroup)
            {
                foreach (var child in ((DrawingGroup)drawing).Children)
                {
                    foreach (var geometry in AllGeometry(child))
                    {
                        yield return geometry;
                    }
                }
            }
        }
    }

}
