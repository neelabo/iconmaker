﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using IconMaker.Layers;

namespace IconMaker
{
    public class MainWindowVM : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([global::System.Runtime.CompilerServices.CallerMemberNameAttribute] string name = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
            }
        }
        #endregion

        public IconGenerator IconGenerator { get; set; }

        public ICommand CommandNew { set; get; }
        public ICommand CommandSave { set; get; }
        public ICommand CommandSaveAs { set; get; }
        public ICommand CommandLoad { set; get; }
        public ICommand CommandExit { set; get; }

        public ICommand CommandExport { set; get; }
        public ICommand CommandExportPng { set; get; }

        public ICommand CommandUndo { set; get; }
        public ICommand CommandRedo { set; get; }

        public ICommand CommandAddLayer { set; get; }
        public ICommand CommandCopyLayer { set; get; }
        public ICommand CommandRemoveLayer { set; get; }
        public ICommand CommandMoveUpLayer { set; get; }
        public ICommand CommandMoveDownLayer { set; get; }

        public ICommand CommandGotoWeb { set; get; }
        public ICommand CommandTest { set; get; }

        public CommandHistoryCustom History { set; get; } = App.History;
        public ICommand CommandUpdateHistory { set; get; }
        public ICommand CommandClearHistory { set; get; }

        #region Property: Level
        private int _Level;
        public int Level
        {
            get { return _Level; }
            set { _Level = value; OnPropertyChanged(); LevelChanged(); }
        }
        #endregion

        #region Property: UserContent
        private UIElement _UserContent;
        public UIElement UserContent
        {
            get { return _UserContent; }
            set { _UserContent = value; OnPropertyChanged(); }
        }
        #endregion

        #region Property: ProjectFilename
        private string _ProjectFilename;
        public string ProjectFileName
        {
            get { return _ProjectFilename; }
            set { _ProjectFilename = value; OnPropertyChanged(); OnPropertyChanged("WindowTitle"); }
        }
        #endregion

        #region Property: ParameterControl
        private UIElement _ParameterControl;
        public UIElement ParameterControl
        {
            get { return _ParameterControl; }
            set { _ParameterControl = value; OnPropertyChanged(); }
        }
        #endregion

        private string _DefaultWindowTitle;

        //
        public string WindowTitle
        {
            get
            {
                return ((ProjectFileName != null) ? System.IO.Path.GetFileName(ProjectFileName) + " - " : "") + _DefaultWindowTitle;
            }
        }


        /// <summary>
        ///
        /// </summary>
        public MainWindowVM()
        {
            // title
            var assembly = global::System.Reflection.Assembly.GetExecutingAssembly();
            var ver = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            _DefaultWindowTitle = $"{assembly.GetName().Name} {ver.FileMajorPart}.{ver.ProductMinorPart}";
#if DEBUG
            _DefaultWindowTitle += " [Debug]";
#endif

            // models
            IconGenerator = new IconGenerator();

            // commands
            CommandAddLayer = new RelayCommand<LayerType>(AddLayer);

            CommandCopyLayer = new RelayCommand<Layer>(CopyLayer, IsSelectedLayer);
            CommandRemoveLayer = new RelayCommand<Layer>(RemoveLayer, IsSelectedLayer);
            CommandMoveUpLayer = new RelayCommand<Layer>(MoveUpLayer, IsSelectedLayer);
            CommandMoveDownLayer = new RelayCommand<Layer>(MoveDownLayer, IsSelectedLayer);

            CommandGotoWeb = new RelayCommand(() => CallBrowser("https://bitbucket.org/neelabo/iconmaker/wiki/"));

            CommandUndo = new RelayCommand(() => App.History.Undo());
            CommandRedo = new RelayCommand(() => App.History.Redo());
            CommandClearHistory = new RelayCommand(() => App.History.Clear());
            CommandUpdateHistory = new RelayCommand(() => OnPropertyChanged(nameof(History)));


            IconGenerator.PropertyChanged += IconGenerator_PropertyChanged;

            Level = 0;
        }


        private void CallBrowser(string address)
        {
            var info = new System.Diagnostics.ProcessStartInfo();
            info.FileName = address;
            info.UseShellExecute = true;
            System.Diagnostics.Process.Start(info);
        }

        //
        private bool IsSelectedLayer(Layer layer)
        {
            return (layer != null);
        }

        //
        private void IconGenerator_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedLayer") // 選択レイヤー変更
            {
                UpdateLayerPropertyUI();
            }
        }

        /// <summary>
        /// パラメータ編集ウィンドウの表示更新
        /// </summary>
        public void UpdateLayerPropertyUI()
        {
            if (IconGenerator.SelectedLayer != null)
            {
                ParameterControl = IconGenerator.SelectedLayer.CreatePropertyEditerUIElement();
            }
            else
            {
                ParameterControl = null;
            }
        }

        //
        public void Initialize()
        {
            ProjectFileName = null;
            IconGenerator.Initialize();
            Level = 0;
        }

        //
        public void Save(string path)
        {
            IconGenerator.Save(path);
            ProjectFileName = path;
        }

        //
        public void Load(string path)
        {
            IconGenerator.Load(path);
            ProjectFileName = path;

            IconGenerator.SelectedLayer = null;

            Level = 0;
        }

        //
        public void ExportIcon(string path)
        {
            IconGenerator.ExportIcon(path);
        }

        //
        public void ExportPng(string path, int level)
        {
            IconGenerator.ExportPng(path, level);
        }

        //
        public void AddLayer(LayerType key)
        {
            IconGenerator.NewLayer(key);
        }


        //
        public void CopyLayer(Layer layer)
        {
            IconGenerator.CopyLayer(layer);
        }

        //
        public void RemoveLayer(Layer layer)
        {
            IconGenerator.RemoveLayer(layer);
        }

        //
        public void MoveUpLayer(Layer layer)
        {
            IconGenerator.MoveUpLayer(layer);
        }

        //
        public void MoveDownLayer(Layer layer)
        {
            IconGenerator.MoveDownLayer(layer);
        }

        public void MoveLayerStart(Point start)
        {
            IconGenerator.MoveLayerStart(start);
        }

        public void MoveLayer(Point start, Point end)
        {
            IconGenerator.MoveLayer(start, end);
        }

        public void MoveLayerEnd()
        {
            IconGenerator.MoveLayerEnd();
        }

        public void MoveLayer(double dx, double dy)
        {
            IconGenerator.MoveLayer(dx, dy);
        }

        public void AngleLayer(Point start, Point end)
        {
            IconGenerator.AngleLayer(start, end);
        }

        public void SizeLayer(Point start, Point end)
        {
            IconGenerator.SizeLayer(start, end);
        }


        private void LevelChanged()
        {
            IconGenerator.SetEditLevel(Level);
            UpdateLayerPropertyUI();
        }
    }


}
