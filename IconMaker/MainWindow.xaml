﻿<!--
    Copyright (c) 2015 Mitsuhiro Ito (nee)

    This software is released under the MIT License.
    http://opensource.org/licenses/mit-license.php
-->
<Window x:Class="IconMaker.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:IconMaker"
        xmlns:local_layers="clr-namespace:IconMaker.Layers"
        xmlns:osc="clr-namespace:OpenSourceControls"
        mc:Ignorable="d"
        Title="{Binding WindowTitle}" Height="640" Width="960" Closing="Window_Closing">

    <Window.Resources>
        <BooleanToVisibilityConverter x:Key="BoolToVisibility"/>
        <local:LevelToBrushConverter x:Key="LevelToBrushConverter" />

        <ControlTemplate x:Key="AddLayerMenu" TargetType="MenuItem">
            <MenuItem Header="Add" DataContext="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=Window}, Path=DataContext}">
                <MenuItem Header="Rectangle" Command="{Binding CommandAddLayer}" CommandParameter="{x:Static local_layers:LayerType.Rectangle}"/>
                <MenuItem Header="Ellipse" Command="{Binding CommandAddLayer}" CommandParameter="{x:Static local_layers:LayerType.Ellipse}"/>
                <MenuItem Header="Text" Command="{Binding CommandAddLayer}" CommandParameter="{x:Static local_layers:LayerType.Text}"/>
                <MenuItem Header="Bitmap" Command="{Binding CommandAddLayer}" CommandParameter="{x:Static local_layers:LayerType.Bitmap}"/>
                <MenuItem Header="SVG" Command="{Binding CommandAddLayer}" CommandParameter="{x:Static local_layers:LayerType.Svg}"/>
            </MenuItem>
        </ControlTemplate>

    </Window.Resources>

    <Window.InputBindings>
        <KeyBinding Gesture="Ctrl+S" Command="{Binding CommandSave}" />
        <KeyBinding Gesture="Ctrl+Z" Command="{Binding CommandUndo}" />
        <KeyBinding Gesture="Ctrl+Y" Command="{Binding CommandRedo}" />
    </Window.InputBindings>

    <DockPanel Background="#FFDDDDDD">

        <Grid DockPanel.Dock="Top">
            <Menu Height="22">
                <MenuItem Header="File(_F)">
                    <MenuItem Header="New" Command="{Binding CommandNew}"/>
                    <MenuItem Header="Open(_O)..." Command="{Binding CommandLoad}"/>
                    <MenuItem Header="Save" Command="{Binding CommandSave}" InputGestureText="Ctrl+S"/>
                    <MenuItem Header="Save As..." Command="{Binding CommandSaveAs}"/>
                    <Separator/>
                    <MenuItem Header="Exit(_X)" Command="{Binding CommandExit}" InputGestureText="Alt+F4"/>
                </MenuItem>

                <MenuItem Header="Icon(_I)">
                    <MenuItem Header="Export ICO..." Command="{Binding CommandExport}"/>
                    <MenuItem Header="Export PNG">
                        <MenuItem Header="256x256" Command="{Binding CommandExportPng}" CommandParameter="1"/>
                        <MenuItem Header="48x48" Command="{Binding CommandExportPng}" CommandParameter="2"/>
                        <MenuItem Header="32x32" Command="{Binding CommandExportPng}" CommandParameter="3"/>
                        <MenuItem Header="16x16" Command="{Binding CommandExportPng}" CommandParameter="4"/>
                    </MenuItem>
                </MenuItem>

                <MenuItem Header="Edit(_E)">
                    <MenuItem Header="Undo(_U)" Command="{Binding CommandUndo}" InputGestureText="Ctrl+Z"/>
                    <MenuItem Header="Redo(_R)" Command="{Binding CommandRedo}"  InputGestureText="Ctrl+Y"/>
                </MenuItem>

                <MenuItem Header="Layer(_L)">
                    <MenuItem Template="{StaticResource AddLayerMenu}"/>
                    <MenuItem Header="Copy(_C)" Command="{Binding CommandCopyLayer}" CommandParameter="{Binding ElementName=LayerListBox, Path=SelectedValue}" InputGestureText="Ctrl+C"/>
                    <Separator/>
                    <MenuItem Header="Move Up" Command="{Binding CommandMoveUpLayer}" CommandParameter="{Binding ElementName=LayerListBox, Path=SelectedValue}" InputGestureText="Alt+Up"/>
                    <MenuItem Header="Move Down" Command="{Binding CommandMoveDownLayer}" CommandParameter="{Binding ElementName=LayerListBox, Path=SelectedValue}" InputGestureText="Alt+Down"/>
                    <Separator/>
                    <MenuItem Header="Delete(_D)" Command="{Binding CommandRemoveLayer}" CommandParameter="{Binding ElementName=LayerListBox, Path=SelectedValue}" InputGestureText="Delete"/>
                </MenuItem>

            </Menu>
            <Button Style="{StaticResource IconButton}" Command="{Binding CommandGotoWeb}" Width="20" Height="20" HorizontalAlignment="Right" Margin="0,1,4,1">
                <Image Width="16" Height="16" Source="{StaticResource ic_help_24px}"/>
            </Button>
        </Grid>

        <DockPanel DockPanel.Dock="Left" Margin="1">

            <TextBlock  DockPanel.Dock="Top" Text="Icon" Foreground="Gray" Background="#FFEEEEEE" Padding="4,2,4,2"/>

            <ListBox DockPanel.Dock="Left" Width="96" x:Name="IconSourceListBox" SelectedIndex="{Binding Level}" ItemsSource="{Binding IconGenerator.IconSources}" BorderBrush="{x:Null}" >
                <ListBox.ItemTemplate>
                    <DataTemplate>
                        <StackPanel  Width="64" Margin="8">
                            <Image Source="{Binding Image}" Width="{Binding DispPixel}" Height="{Binding DispPixel}" />
                            <TextBlock Text="{Binding Label}" HorizontalAlignment="Center" />
                        </StackPanel>
                    </DataTemplate>
                </ListBox.ItemTemplate>
            </ListBox>

        </DockPanel>

        <DockPanel DockPanel.Dock="Right" Width="160" Margin="1">

            <TextBlock DockPanel.Dock="Top" Text="{Binding IconGenerator.EditLevelLabel, StringFormat={}{0} Layers}" Foreground="Gray" Background="#FFEEEEEE" Padding="4,2,4,2" />

            <ListBox x:Name="LayerListBox" ItemsSource="{Binding IconGenerator.Layers}" SelectedItem="{Binding IconGenerator.SelectedLayer}" HorizontalContentAlignment="Stretch" RenderTransformOrigin="0.5,0.5" BorderBrush="{x:Null}" Drop="LayerListBox_Drop" >

                <ListBox.ItemContainerStyle>
                    <Style TargetType="ListBoxItem">
                        <EventSetter Event="PreviewMouseLeftButtonDown" Handler="listBoxItem_PreviewMouseLeftButtonDown"/>
                        <EventSetter Event="PreviewMouseMove" Handler="listBoxItem_PreviewMouseMove"/>
                        <EventSetter Event="QueryContinueDrag" Handler="listBoxItem_QueryContinueDrag"/>
                    </Style>
                </ListBox.ItemContainerStyle>

                <ListBox.ItemTemplate>
                    <DataTemplate>
                        <DockPanel>
                            <CheckBox Style="{StaticResource EyeCheckBox}" Width="16" Height="16" Margin="2" IsChecked="{Binding Common.IsVisible.Value}"/>
                            <Border BorderBrush="#FFEEEEEE" BorderThickness="1" Margin="2">
                                <Canvas Width="32" Height="32" Background="{StaticResource CheckerBrush}" >
                                    <Image DockPanel.Dock="Left" Source="{Binding Thumbnail}" Width="32" Height="32" />
                                </Canvas>
                            </Border>
                            <Grid>
                                <TextBlock Text="{Binding Label}" VerticalAlignment="Center" Margin="2"/>
                                <!--
                                <TextBox Text="{Binding Label}" VerticalAlignment="Center" Margin="0">
                                    <TextBox.Style>
                                        <Style TargetType="TextBox">
                                            <Style.Triggers>
                                                <Trigger Property="IsKeyboardFocused" Value="False">
                                                    <Setter Property="Visibility" Value="Hidden"/>
                                                </Trigger>
                                            </Style.Triggers>
                                        </Style>
                                    </TextBox.Style>
                                </TextBox>
                                -->
                            </Grid>
                        </DockPanel>
                    </DataTemplate>
                </ListBox.ItemTemplate>


                <ListBox.InputBindings>
                    <KeyBinding Gesture="Ctrl+C" Command="{Binding IconGenerator.CommandCopy}" />
                    <KeyBinding Gesture="Delete" Command="{Binding IconGenerator.CommandRemove}"/>
                    <KeyBinding Gesture="Alt+Up" Command="{Binding IconGenerator.CommandMoveUp}" />
                    <KeyBinding Gesture="Alt+Down" Command="{Binding IconGenerator.CommandMoveDown}" />
                </ListBox.InputBindings>

                <ListBox.ContextMenu>
                    <ContextMenu>
                        <MenuItem Template="{StaticResource AddLayerMenu}"/>
                        <MenuItem Header="Copy(_C)" Command="{Binding IconGenerator.CommandCopy}" InputGestureText="Ctrl+C"/>
                        <Separator/>
                        <!--<MenuItem Header="Rename" Command="{Binding IconGenerator.CommandRename}" InputGestureText="F2"/>-->
                        <MenuItem Header="Move Up" Command="{Binding IconGenerator.CommandMoveUp}" InputGestureText="Alt+Up"/>
                        <MenuItem Header="Move Down" Command="{Binding IconGenerator.CommandMoveDown}" InputGestureText="Alt+Down"/>
                        <Separator/>
                        <MenuItem Header="Delete(_D)" Command="{Binding IconGenerator.CommandRemove}" InputGestureText="Delete"/>
                    </ContextMenu>
                </ListBox.ContextMenu>

            </ListBox>

        </DockPanel>

        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition Width="300" />
            </Grid.ColumnDefinitions>

            <GridSplitter Grid.Column="0" Width="4"/>

            <DockPanel Grid.Column="1" DockPanel.Dock="Right" Margin="1" Background="#FFEEEEEE">

                <DockPanel DockPanel.Dock="Top"  Background="#FFEEEEEE" LastChildFill="False">
                    <Border DockPanel.Dock="Left"  Background="{StaticResource CheckerBrush}" Width="56" Height="56" BorderThickness="0,0,1,1" >
                        <Border.BorderBrush>
                            <SolidColorBrush Color="#FFD8D8D8"/>
                        </Border.BorderBrush>
                        <Image Source="{Binding IconGenerator.EditLayer.Thumbnail}" Width="48" Height="48" />
                    </Border>
                    <StackPanel DockPanel.Dock="Bottom" Orientation="Horizontal" Margin="8,4,8,8">
                        <TextBlock Text="{Binding IconGenerator.EditLevelLabel, StringFormat={}{0} -}" />
                        <TextBlock Text="{Binding IconGenerator.SelectedLayer.TypeName}" Margin="4,0,0,0"/>
                        <!--<TextBlock Text="{Binding IconGenerator.SelectedLayer.ID}" Margin="8,0,0,0"/>-->
                    </StackPanel>
                    <TextBox DockPanel.Dock="Bottom" Text="{Binding IconGenerator.SelectedLayer.Label}" Margin="8,8,8,4" VerticalAlignment="Bottom" />

                    <DockPanel.Style>
                        <Style TargetType="DockPanel">
                            <Style.Triggers>
                                <DataTrigger Binding="{Binding IconGenerator.SelectedLayer}" Value="{x:Null}">
                                    <Setter Property="Visibility" Value="Hidden"/>
                                </DataTrigger>
                            </Style.Triggers>
                        </Style>
                    </DockPanel.Style>
                </DockPanel>
                
                <ContentControl>
                    <ScrollViewer VerticalScrollBarVisibility="Auto" x:Name="LayerProperty" Background="{Binding IconGenerator.EditLayer.Level, Converter={StaticResource LevelToBrushConverter}}" Content="{Binding ParameterControl}"/>
                </ContentControl>

            </DockPanel>

            <DockPanel Grid.Column="0" Margin="0,0,4,0">

                <!-- Undo デバッグ用
                <Grid DockPanel.Dock="Bottom" Height="79">
                    <Button Content="Undo" Width="50" Height="24" Command="{Binding CommandUndo}" Margin="10,0,0,39" VerticalAlignment="Bottom" HorizontalAlignment="Left" />
                    <Button Content="Redo" Width="50" Height="24" Command="{Binding CommandRedo}" Margin="10,0,0,10" VerticalAlignment="Bottom" HorizontalAlignment="Left" />
                    <Button Content="Update" Width="50" Height="24" Command="{Binding CommandUpdateHistory}" HorizontalAlignment="Left" Margin="120,0,0,39" VerticalAlignment="Bottom"/>
                    <TextBlock Text="{Binding History.UndoCount}" Width="50" Height="24" Margin="65,0,0,39" VerticalAlignment="Bottom" HorizontalAlignment="Left"/>
                    <TextBlock Text="{Binding History.RedoCount}" Width="50" Height="24" Margin="65,0,0,10" VerticalAlignment="Bottom" HorizontalAlignment="Left"/>
                    <Button Content="Clear" Width="50" Height="24" Command="{Binding CommandClearHistory}" HorizontalAlignment="Left" Margin="120,0,0,10" VerticalAlignment="Bottom"/>
                </Grid>
                -->

                <ScrollViewer x:Name="MainView" DockPanel.Dock="Left"  HorizontalScrollBarVisibility="Auto" VerticalScrollBarVisibility="Auto" Margin="1"
                Background="{StaticResource CheckerBrush}"
                PreviewMouseLeftButtonDown="OnMouseLeftButtonDown" PreviewMouseLeftButtonUp="OnMouseLeftButtonUp" PreviewMouseMove="OnMouseMove"
                PreviewKeyDown="OnPreviewKeyDown" PreviewKeyUp="OnPreviewKeyUp" LostKeyboardFocus="OnLostKeyboardFocus" >

                    <Grid x:Name="BackGroundGrid" Width="256" Height="256" Background="#22D3D3D3">

                        <Image x:Name="MainImage" Width="256" Height="256" Source="{Binding ElementName=IconSourceListBox, Path=SelectedValue.Image}"
                           RenderOptions.BitmapScalingMode="{Binding ElementName=IconSourceListBox, Path=SelectedValue.ViewScalingMode}"
                           RenderTransformOrigin="0.5,0.5"/>

                        <Canvas Width="256" Height="256" Visibility="{Binding ElementName=MainView, Path=IsFocused, Converter={StaticResource BoolToVisibility}}">
                            <Rectangle DataContext="{Binding IconGenerator.EditLayer}"
                                   Canvas.Top="{Binding Common.Top.Value}"
                                   Canvas.Left="{Binding Common.Left.Value}"
                                Width="{Binding Common.Width.Value}"
                                Height="{Binding Common.Height.Value}"
                                Stroke="Aqua" StrokeThickness="0.5" RenderTransformOrigin="0.5,0.5">
                                <Rectangle.RenderTransform>
                                    <TransformGroup>
                                        <ScaleTransform/>
                                        <SkewTransform/>
                                        <RotateTransform Angle="{Binding Common.Angle.Value}"/>
                                        <TranslateTransform/>
                                    </TransformGroup>
                                </Rectangle.RenderTransform>
                            </Rectangle>
                        </Canvas>

                    </Grid>

                </ScrollViewer>


            </DockPanel>
        </Grid>

    </DockPanel>
</Window>
