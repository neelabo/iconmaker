﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IconMaker
{
    /// <summary>
    /// Command履歴
    /// </summary>
    public class CommandHistory
    {
        protected MementoCommandStack _UndoStack;
        protected MementoCommandStack _RedoStack;

        private Stack<IMementoCommand> _GroupStack;

        private IMementoCommand _Terminator;


        private int _LockCount;


        public void Lock()
        {
            _LockCount++;
        }

        public void Unlock()
        {
            _LockCount--;
        }

        public CommandHistory()
        {
            _UndoStack = new MementoCommandStack();
            _RedoStack = new MementoCommandStack();

            _GroupStack = new Stack<IMementoCommand>();

            _Terminator = new MementoNone();

            _UndoStack.Push(_Terminator);
        }


        public void BeginGroup()
        {
            _GroupStack.Push(_UndoStack.Peek());
        }

        public void EndGroup()
        {
            if (_GroupStack.Count <= 0) return;

            var marker = _GroupStack.Pop();
            if (_UndoStack.Peek() == marker) return;

            // マーカー位置まで取り出す
            var group = new List<IMementoCommand>();
            while (_UndoStack.Peek() != marker)
            {
                group.Add(_UndoStack.Pop());
            }

            // 圧縮
            int index = 0;
            while (index < group.Count - 1)
            {
                var v1 = group[index];
                for (int i = index + 1; i < group.Count; ++i)
                {
                    var v0 = group[i];
                    if (v1.EqualsTarget(v0))
                    {
                        v1.Merge(v0);
                        group.Remove(v0);
                        i--;
                    }
                }
                index++;
            }

            // 並び替えてスタックにする
            var groupStack = new MementoCommandStack();
            foreach (var e in group.Reverse<IMementoCommand>())
            {
                groupStack.Push(e);
            }

            _UndoStack.Push(groupStack);
        }


        public void Add(IMementoCommand command)
        {
            if (_LockCount > 0) return;

            Lock();
            _RedoStack.Clear();
            _UndoStack.Push(command);
            Unlock();
        }

        public bool CanUndo()
        {
            return (_UndoStack.Count > 0 && _UndoStack.Peek() != _Terminator);
        }

        public void Undo()
        {
            if (!CanUndo()) return;

            Lock();
            var command = _UndoStack.Pop();
            command.Undo();
            _RedoStack.Push(command);
            Unlock();
        }

        public bool CanRedo()
        {
            return (_RedoStack.Count > 0 && _RedoStack.Peek() != _Terminator);
        }

        public void Redo()
        {
            if (!CanRedo()) return;
            Lock();
            var command = _RedoStack.Pop();
            command.Redo();
            _UndoStack.Push(command);
            Unlock();
        }

        public void Clear()
        {
            _LockCount = 0;

            _UndoStack.Clear();
            _RedoStack.Clear();

            _UndoStack.Push(_Terminator);
        }
    }

    /// <summary>
    /// MementoCommand Interface
    /// </summary>
    public interface IMementoCommand
    {
        void Undo();
        void Redo();
        bool EqualsTarget(IMementoCommand target);
        void Merge(IMementoCommand target);
    }

    /// <summary>
    /// Stacked MementoCommand
    /// </summary>
    public class MementoCommandStack : Stack<IMementoCommand>, IMementoCommand
    {
        public void Redo()
        {
            foreach (var e in this) { e.Redo(); }
        }

        public void Undo()
        {
            foreach (var e in this) { e.Undo(); }
        }

        public bool EqualsTarget(IMementoCommand target)
        {
            return (this.GetType() == target.GetType());
        }

        public void Merge(IMementoCommand target)
        {
            foreach (var e in ((MementoCommandStack)target).ToList().Reverse<IMementoCommand>())
            {
                this.Push(e);
            }
        }
    }

    /// <summary>
    /// Standard MementoCommand (abstract)
    /// </summary>
    /// <typeparam name="T1">target type</typeparam>
    /// <typeparam name="T2">value type</typeparam>
    public abstract class MementoCommand<T1, T2> : IMementoCommand
    {
        protected T1 Target { get; set; }
        public T2 Value0 { get; protected set; }
        public T2 Value1 { get; protected set; }

        public MementoCommand() { }

        public MementoCommand(T1 target, T2 v0,T2 v1)
        {
            Target = target;
            Value0 = v0;
            Value1 = v1;
        }

        public abstract void Undo();
        public abstract void Redo();

        public virtual bool EqualsTarget(IMementoCommand target)
        {
            return false;
        }

        public virtual void Merge(IMementoCommand target)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// None MementoCommand
    /// </summary>
    public class MementoNone : IMementoCommand
    {
        public void Redo()
        {
        }

        public void Undo()
        {
        }

        public bool EqualsTarget(IMementoCommand target)
        {
            return false;
        }

        public void Merge(IMementoCommand target)
        {
            throw new NotImplementedException();
        }
    }
}
