-----
DLL:
Svg2Xaml.dll

Svg2Xaml
URL: http://svg2xaml.codeplex.com/
License: LGPL

-----
Code in:
IconMaker/ComboColorPicker

Small ColorPicker for WPF
URL: http://www.codeproject.com/Articles/34376/Small-ColorPicker-for-WPF
License: CPOL

-----
Code in:
IconMaker/IconBitmapEncoder

A High-Quality IconBitmapEncoder for WPF
URL: http://www.codeproject.com/Tips/687057/A-High-Quality-IconBitmapEncoder-for-WPF
License: CPOL

-----
ICON:
ic_help_24px etc...

Material design icons
URL: http://www.google.com/design/spec/style/icons.html#icons-system-icons
License: CC-BY 4.0