﻿/*
A High-Quality IconBitmapEncoder for WPF
http://www.codeproject.com/Tips/687057/A-High-Quality-IconBitmapEncoder-for-WPF

This article, along with any associated source code and files, is licensed under The Code Project Open License(CPOL) 1.02
http://www.codeproject.com/info/cpol10.aspx

---
Modifyed by Mitsuhiro Ito (nee), 2015

- IconBitmapFramesCollection を List<IconFrame> に変更
- 出力サイズが大きすぎたのを修正
- マスク対応
- パレット選択方法変更
- biSizeImage出力

*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HL.CSharp.Wpf.Icons
{
    public class IconBitmapEncoder : System.Windows.Threading.DispatcherObject
    {
        public List<IconFrame> _Frames = new List<IconFrame>();
        public List<IconFrame> Frames
        {
            get { return _Frames; }
        }

        #region "Methods"

        public void Save(System.IO.Stream Stream)
        {
            SortAscending(Frames); // frame

            System.IO.BinaryWriter writer = new System.IO.BinaryWriter(Stream, System.Text.Encoding.UTF32);

            ushort FramesCount = Convert.ToUInt16(Frames.Count);
            ushort FileHeaderLength = 6;
            ushort FrameHeaderLength = 16;

            ICONDIR FileHeader = new ICONDIR(FramesCount);
            writer.Write(FileHeader.idReserved);
            writer.Write(FileHeader.idType);
            writer.Write(FileHeader.idCount);

            byte[][] data = new byte[FramesCount][];

            foreach (IconFrame frame in Frames)
            {
                int FrameIndex = Frames.IndexOf(frame);
                if (frame.PixelWidth == 256)
                {
                    data[FrameIndex] = GetPNGData(frame);
                }
                else
                {
                    data[FrameIndex] = GetBMPData(frame);
                }
            }

            uint FrameDataOffset = FileHeaderLength;
            FrameDataOffset += (uint)(FrameHeaderLength * FramesCount);

            foreach (IconFrame frame in Frames)
            {
                int FrameIndex = Frames.IndexOf(frame);
                if (FrameIndex > 0)
                {
                    FrameDataOffset += Convert.ToUInt32(data[FrameIndex - 1].Length);
                }
                ICONDIRENTRY FrameHeader = new ICONDIRENTRY((ushort)frame.PixelWidth, (ushort)frame.PixelHeight, Convert.ToUInt16(frame.BitmapFrame.Format.BitsPerPixel), Convert.ToUInt32(data[FrameIndex].Length), FrameDataOffset);
                writer.Write(FrameHeader.bWidth);
                writer.Write(FrameHeader.bHeight);
                writer.Write(FrameHeader.bColorCount);
                writer.Write(FrameHeader.bReserved);
                writer.Write(FrameHeader.wPlanes);
                writer.Write(FrameHeader.wBitCount);
                writer.Write(FrameHeader.dwBytesInRes);
                writer.Write(FrameHeader.dwImageOffset);
            }

            foreach (byte[] frameData in data)
            {
                writer.Write(frameData);
            }

        }

        //
        public void SortAscending(List<IconFrame> frames)
        {
            frames.Sort(new Comparison<IconFrame>((IconFrame x, IconFrame y) =>
            {
                if (x.BitmapFrame.PixelWidth > y.BitmapFrame.PixelWidth)
                {
                    return 1;
                }
                else if (x.BitmapFrame.PixelWidth < y.BitmapFrame.PixelWidth)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }));
        }

        //
        private byte[] GetPNGData(IconFrame frame)
        {
            System.IO.MemoryStream DataStream = new System.IO.MemoryStream();
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(frame.BitmapFrame);
            encoder.Save(DataStream);
            encoder = null;
            byte[] Data = DataStream.GetBuffer();
            DataStream.Close();
            return Data;
        }

        //
        private byte[] GetBMPData(IconFrame frame)
        {
            System.IO.MemoryStream DataStream = new System.IO.MemoryStream();
            BmpBitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(frame.BitmapFrame);
            encoder.Save(DataStream);
            encoder = null;
            DataStream.Position = 14;
            System.IO.BinaryReader DataStreamReader = new System.IO.BinaryReader(DataStream, System.Text.UTF32Encoding.UTF32);
            System.IO.MemoryStream OutDataStream = new System.IO.MemoryStream();
            System.IO.BinaryWriter OutDataStreamWriter = new System.IO.BinaryWriter(OutDataStream, System.Text.UTF32Encoding.UTF32);
            OutDataStreamWriter.Write(DataStreamReader.ReadUInt32()); // bcSize:4
            OutDataStreamWriter.Write(DataStreamReader.ReadInt32()); // bcWidth:4
            OutDataStreamWriter.Write(DataStreamReader.ReadInt32() * 2); // bcHeight:4 ... なんで２倍にするのだろう？
            OutDataStreamWriter.Write(DataStreamReader.ReadInt16()); // biPlanes:2
            OutDataStreamWriter.Write(DataStreamReader.ReadInt16()); // biBitCount:2
            OutDataStreamWriter.Write(DataStreamReader.ReadUInt32()); // biCompression:4
            long biSizeImagePos = OutDataStream.Position;
            OutDataStreamWriter.Write(DataStreamReader.ReadUInt32()); // biSizeImage:4
            OutDataStreamWriter.Write((int)0); DataStreamReader.ReadInt32();// biXPixelsPerMeter:4
            OutDataStreamWriter.Write((int)0); DataStreamReader.ReadInt32();// biYPixelsPerMeter:4
            OutDataStreamWriter.Write(DataStreamReader.ReadUInt32()); // biClrUsed:4
            OutDataStreamWriter.Write(DataStreamReader.ReadUInt32()); // biClrImpotant:4

            long imageStart = OutDataStream.Position;

            // palette,pixels
            int b;
            while ((b = DataStream.ReadByte()) >= 0)
            {
                OutDataStream.WriteByte((byte)b);
            }

            // mask
            if (frame.Mask != null)
            {
                OutDataStream.Write(frame.Mask, 0, frame.Mask.Length);
            }

            long imageEnd = OutDataStream.Position;

            // overwrite biSizeImage
            OutDataStream.Position = biSizeImagePos;
            OutDataStreamWriter.Write((uint)(imageEnd - imageStart));

            byte[] data = OutDataStream.ToArray();

            OutDataStreamWriter.Close();
            OutDataStream.Close();
            DataStreamReader.Close();
            DataStream.Close();

            return data;
        }


        #endregion

        #region "Icon File Structures"

        private struct ICONDIR
        {

            //Reserved (must be 0)
            public readonly ushort idReserved;
            //Resource Type (1 for icons)
            public readonly ushort idType;
            //How many images?

            public readonly ushort idCount;
            public ICONDIR(ushort Count)
            {
                idReserved = Convert.ToUInt16(0);
                idType = Convert.ToUInt16(1);
                idCount = Count;
            }

        }

        private struct ICONDIRENTRY
        {

            //Width, in pixels, of the image
            public readonly byte bWidth;
            //Height, in pixels, of the image
            public readonly byte bHeight;
            //Number of colors in image (0 if >=8bpp)
            public readonly byte bColorCount;
            //Reserved ( must be 0)
            public readonly byte bReserved;
            //Color Planes
            public readonly ushort wPlanes;
            //Bits per pixel
            public readonly ushort wBitCount;
            //How many bytes in this resource?
            public readonly uint dwBytesInRes;
            //Where in the file is this image?

            public readonly uint dwImageOffset;
            public ICONDIRENTRY(ushort Width, ushort Height, ushort BitsPerPixel, uint ResSize, uint ImageOffset)
            {
                if (Width == 256)
                {
                    bWidth = Convert.ToByte(0);
                }
                else
                {
                    bWidth = Convert.ToByte(Width);
                }
                if (Height == 256)
                {
                    bHeight = Convert.ToByte(0);
                }
                else
                {
                    bHeight = Convert.ToByte(Height);
                }
                if (BitsPerPixel == 4)
                {
                    bColorCount = Convert.ToByte(16);
                }
                else
                {
                    bColorCount = Convert.ToByte(0);
                }
                bReserved = Convert.ToByte(0);
                wPlanes = Convert.ToUInt16(1);
                wBitCount = BitsPerPixel;
                dwBytesInRes = ResSize;
                dwImageOffset = ImageOffset;
            }

        }

        #endregion

        #region "Helpers"

        public static BitmapSource Get4BitImage(BitmapSource Source)
        {
            var palette = new BitmapPalette(Source, 16);
            FormatConvertedBitmap @out = new FormatConvertedBitmap(Source, PixelFormats.Indexed4, palette, 0);

            return @out;
        }

        public static BitmapSource Get8BitImage(BitmapSource Source)
        {
            var palette = new BitmapPalette(Source, 256);
            FormatConvertedBitmap @out = new FormatConvertedBitmap(Source, PixelFormats.Indexed8, palette, 0);

            return @out;
        }

        public static BitmapSource Get24plus8BitImage(BitmapSource Source)
        {
            FormatConvertedBitmap @out = new FormatConvertedBitmap(Source, PixelFormats.Bgra32, null, 0);
            return @out;
        }

        #endregion
    }


    //
    public class IconFrame
    {
        public BitmapFrame BitmapFrame { get; set; }
        public byte[] Mask { get; set; }

        public int PixelWidth => BitmapFrame.PixelWidth;
        public int PixelHeight => BitmapFrame.PixelHeight;

        public static IconFrame Create(BitmapFrame bmp, byte[] mask)
        {
            return new IconFrame() { BitmapFrame = bmp, Mask = mask };
        }
    }
}
