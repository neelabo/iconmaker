﻿// ドラッグによるListBoxItemの入れ替え
// from http://main.tinyjoker.net/Tech/CSharp/WPF/ListBox%A4%CE%A5%A2%A5%A4%A5%C6%A5%E0%A4%F2%C8%BE%C6%A9%CC%C0%A5%B4%A1%BC%A5%B9%A5%C8%A4%C4%A4%AD%A5%C9%A5%E9%A5%C3%A5%B0%A5%A2%A5%F3%A5%C9%A5%C9%A5%ED%A5%C3%A5%D7%A4%C7%CA%C2%A4%D9%C2%D8%A4%A8%A4%EB.html
//
// WIN32APIの高DPI対応
// from http://grabacr.net/archives/1105

using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Shapes;

namespace IconMaker
{
    /// <summary>
    /// ListBoxItemのドラッグによる並び替えを行う
    /// </summary>
    public partial class MainWindow : Window
    {
        ListBoxItem _DragItem;
        Point _DragStartPos;
        DragAdorner _DragGhost;

        private void listBoxItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // マウスダウンされたアイテムを記憶
            _DragItem = sender as ListBoxItem;
            // マウスダウン時の座標を取得
            _DragStartPos = e.GetPosition(_DragItem);
        }

        private void listBoxItem_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            var lbi = sender as ListBoxItem;
            if (e.LeftButton == MouseButtonState.Pressed && _DragGhost == null && _DragItem == lbi)
            {
                var nowPos = e.GetPosition(lbi);
                if (Math.Abs(nowPos.X - _DragStartPos.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(nowPos.Y - _DragStartPos.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    LayerListBox.AllowDrop = true;

                    var layer = AdornerLayer.GetAdornerLayer(LayerListBox);
                    _DragGhost = new DragAdorner(LayerListBox, lbi, 0.5, _DragStartPos);
                    layer.Add(_DragGhost);
                    DragDrop.DoDragDrop(lbi, lbi, DragDropEffects.Move);
                    layer.Remove(_DragGhost);
                    _DragGhost = null;
                    _DragItem = null;

                    LayerListBox.AllowDrop = false;
                }
            }
        }

        private void listBoxItem_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (_DragGhost != null)
            {
                var p = CursorInfo.GetNowPosition(this);
                var loc = this.PointFromScreen(LayerListBox.PointToScreen(new Point(0, 0)));
                _DragGhost.LeftOffset = p.X - loc.X;
                _DragGhost.TopOffset = p.Y - loc.Y;
            }
        }

        private void LayerListBox_Drop(object sender, DragEventArgs e)
        {
            var dropPos = e.GetPosition(LayerListBox);
            var lbi = e.Data.GetData(typeof(ListBoxItem)) as ListBoxItem;
            var o = lbi.DataContext as Layers.Layer;
            var index = VM.IconGenerator.Layers.IndexOf(o);
            for (int i = 0; i < VM.IconGenerator.Layers.Count; i++)
            {
                var item = LayerListBox.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
                var pos = LayerListBox.PointFromScreen(item.PointToScreen(new Point(0, item.ActualHeight / 2)));
                if (dropPos.Y < pos.Y)
                {
                    // i が入れ換え先のインデックス
                    //VM.IconGenerator.Layers.Move(index, (index < i) ? i - 1 : i);
                    VM.IconGenerator.MoveIndexLayer(o, (index < i) ? i - 1 : i);
                    return;
                }
            }
            // 最後にもっていく
            int last = VM.IconGenerator.Layers.Count - 1;
            //VM.IconGenerator.Layers.Move(index, last);
            VM.IconGenerator.MoveIndexLayer(o, last);
        }
    }


    /// <summary>
    /// ドラッグアイテムを半透明表示する
    /// </summary>
    internal class DragAdorner : Adorner
    {
        protected UIElement _child;
        protected double XCenter;
        protected double YCenter;

        public DragAdorner(UIElement owner) : base(owner) { }

        public DragAdorner(UIElement owner, UIElement adornElement, double opacity, Point dragPos)
            : base(owner)
        {
            var _brush = new VisualBrush(adornElement) { Opacity = opacity };
            var b = VisualTreeHelper.GetDescendantBounds(adornElement);
            var r = new Rectangle() { Width = b.Width, Height = b.Height };

            XCenter = dragPos.X;// r.Width / 2;
            YCenter = dragPos.Y;// r.Height / 2;

            r.Fill = _brush;
            _child = r;
        }


        private double _leftOffset;
        public double LeftOffset
        {
            get { return _leftOffset; }
            set
            {
                _leftOffset = value - XCenter;
                UpdatePosition();
            }
        }

        private double _topOffset;
        public double TopOffset
        {
            get { return _topOffset; }
            set
            {
                _topOffset = value - YCenter;
                UpdatePosition();
            }
        }

        private void UpdatePosition()
        {
            var adorner = this.Parent as AdornerLayer;
            if (adorner != null)
            {
                adorner.Update(this.AdornedElement);
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            return _child;
        }

        protected override int VisualChildrenCount
        {
            get { return 1; }
        }

        protected override Size MeasureOverride(Size finalSize)
        {
            _child.Measure(finalSize);
            return _child.DesiredSize;
        }
        protected override Size ArrangeOverride(Size finalSize)
        {

            _child.Arrange(new Rect(_child.DesiredSize));
            return finalSize;
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            var result = new GeneralTransformGroup();
            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform(_leftOffset, _topOffset));
            return result;
        }
    }


    /// <summary>
    /// マウス座標を得る
    /// </summary>
    internal static class CursorInfo
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern void GetCursorPos(out POINT pt);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int ScreenToClient(IntPtr hwnd, ref POINT pt);

        private struct POINT
        {
            public UInt32 X;
            public UInt32 Y;
        }

        public static Point GetNowPosition(Visual v)
        {
            POINT p;
            GetCursorPos(out p);

            var source = HwndSource.FromVisual(v) as HwndSource;
            var hwnd = source.Handle;

            ScreenToClient(hwnd, ref p);

            var dpiScaleFactor = v.GetDpiScaleFactor();
            return new Point(p.X / dpiScaleFactor.X, p.Y / dpiScaleFactor.Y);
        }

        /// <summary>
        /// 現在の <see cref="T:System.Windows.Media.Visual"/> から、DPI 倍率を取得します。
        /// </summary>
        /// <returns>
        /// X 軸 および Y 軸それぞれの DPI 倍率を表す <see cref="T:System.Windows.Point"/>
        /// 構造体。取得に失敗した場合、(1.0, 1.0) を返します。
        /// </returns>
        public static Point GetDpiScaleFactor(this Visual visual)
        {
            var source = PresentationSource.FromVisual(visual);
            if (source != null && source.CompositionTarget != null)
            {
                return new Point(
                    source.CompositionTarget.TransformToDevice.M11,
                    source.CompositionTarget.TransformToDevice.M22);
            }

            return new Point(1.0, 1.0);
        }
    }
}