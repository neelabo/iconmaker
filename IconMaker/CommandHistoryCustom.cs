﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using System.ComponentModel;

namespace IconMaker
{
    public class CommandHistoryCustom : CommandHistory, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event global::System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([global::System.Runtime.CompilerServices.CallerMemberNameAttribute] string name = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
            }
        }
        #endregion

        public int UndoCount { get { return _UndoStack.Count; } }
        public int RedoCount { get { return _RedoStack.Count; } }

        private void OnPropertyChangedAll()
        {
            OnPropertyChanged(nameof(UndoCount));
            OnPropertyChanged(nameof(RedoCount));
        }

        bool _Changed;

        public bool IsChanged()
        {
            return _Changed && CanUndo();
        }


        public void ResetChangeFlag()
        {
            _Changed = false;
        }


        public new void Add(IMementoCommand command)
        {
            base.Add(command);
            OnPropertyChangedAll();
            _Changed = true;
        }

        public new void EndGroup()
        {
            base.EndGroup();
            OnPropertyChangedAll();
        }

        public new void Undo()
        {
            base.Undo();
            OnPropertyChangedAll();
        }

        public new void Redo()
        {
            base.Redo();
            OnPropertyChangedAll();
        }

        public new void Clear()
        {
            base.Clear();
            OnPropertyChangedAll();
        }
    }


    // App に インスタンス定義
    public partial class App : Application
    {
        public static CommandHistoryCustom History { get; private set; } = new CommandHistoryCustom();
    }
}
