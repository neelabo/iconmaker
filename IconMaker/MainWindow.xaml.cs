﻿/*
Copyright (c) 2015 Mitsuhiro Ito (nee)

This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

////using System.Windows.Media.Composition;
using System.Diagnostics;

namespace IconMaker
{

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowVM VM { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            VM = new MainWindowVM();
            VM.Initialize();
            DataContext = VM;

            // commands
            VM.CommandNew = new RelayCommand(NewProject);
            VM.CommandSave = new RelayCommand(Save);
            VM.CommandSaveAs = new RelayCommand(SaveAs);
            VM.CommandLoad = new RelayCommand(Load);
            VM.CommandExit = new RelayCommand(() => Close());

            VM.CommandExport = new RelayCommand(Export);
            VM.CommandExportPng = new RelayCommand<string>(ExportPng);

            this.IconSourceListBox.SelectedIndex = 0;
        }


        /// <summary>
        /// アイコンファイルの出力
        /// </summary>
        void Export()
        {
            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Title = "アイコンファイルの出力";
            dialog.OverwritePrompt = true;
            dialog.Filter = "Icon file|*.ico";

            var result = dialog.ShowDialog();
            if (result == true)
            {
                VM.ExportIcon(dialog.FileName);
            }
        }

        /// <summary>
        /// PNGファイルの出力
        /// </summary>
        void ExportPng(string _level)
        {
            int level = int.Parse(_level);

            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Title = "PNGファイルの出力";
            dialog.OverwritePrompt = true;
            dialog.Filter = "PNG file|*.png";

            var result = dialog.ShowDialog();
            if (result == true)
            {
                VM.ExportPng(dialog.FileName, level);
            }
        }


        /// <summary>
        /// 新規プロジェクト
        /// </summary>
        void NewProject()
        {
            // 破棄確認
            if (App.History.IsChanged())
            {
                var confirm = MessageBox.Show("プロジェクトが保存されていません。\nこのまま新しいプロジェクトを作成してよろしいですか？", "新規プロジェクト", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (confirm != MessageBoxResult.OK)
                {
                    return;
                }
            }

            VM.Initialize();
        }


            /// <summary>
            /// プロジェクトファイルの保存
            /// </summary>
            void Save()
        {
            if (VM.ProjectFileName != null)
            {
                SaveCore(VM.ProjectFileName);
            }
            else
            {
                SaveAs();
            }
        }

        /// <summary>
        /// プロジェクトファイルの保存
        /// </summary>
        void SaveAs()
        {
            var dialog = new SaveFileDialog();

            dialog.Title = "プロジェクトファイルの保存";
            dialog.OverwritePrompt = true;
            dialog.DefaultExt = "*.improj";
            dialog.Filter = "Project File|*.improj";

            var result = dialog.ShowDialog();
            if (result == true)
            {
                SaveCore(dialog.FileName);
            }
        }

        //
        void SaveCore(string path)
        {
            try
            {
                VM.Save(path);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "保存エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        /// <summary>
        /// プロジェクトファイルの読み込み
        /// </summary>
        void Load()
        {
            // 破棄確認
            if (App.History.IsChanged())
            {
                var confirm = MessageBox.Show("プロジェクトが保存されていません。\nこのまま他のプロジェクトを読み込んでよろしいですか？", "読込確認", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (confirm != MessageBoxResult.OK)
                {
                    return;
                }
            }

            var dialog = new OpenFileDialog();

            dialog.Title = "プロジェクトファイルの読み込み";
            dialog.DefaultExt = "*.improj";
            dialog.Filter = "Project File|*.improj";

            var result = dialog.ShowDialog();
            if (result == true)
            {
                try
                {
                    VM.Load(dialog.FileName);
                    this.IconSourceListBox.SelectedIndex = 0;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "読み込みエラー", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }


        #region MouseMove

        private Point DragStartPoint;
        private Point DragEndPoint;
        private bool IsDragging = false;
        private bool IsDragDowned = false;
        private bool IsDragAngle { get { return DragMode == (1 << 0); } }
        private bool IsDragSize { get { return DragMode == (1 << 1); } }
        private int DragMode;

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragStartPoint = DragEndPoint = e.GetPosition(MainImage);

            VM.MoveLayerStart(DragStartPoint);

            IsDragging = true;
            IsDragDowned = false;

            DragMode = 0;
            if ((Keyboard.Modifiers & ModifierKeys.Shift) > 0) DragMode |= (1 << 0);
            if ((Keyboard.Modifiers & ModifierKeys.Control) > 0) DragMode |= (1 << 1);

            MainImage.CaptureMouse();
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!IsDragging) return;

            VM.MoveLayerEnd();
            IsDragging = false;

            MainImage.ReleaseMouseCapture();
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!IsDragging) return;

            DragEndPoint = e.GetPosition(MainImage);

            if (!IsDragDowned)
            {
                IsDragDowned = (Math.Abs(DragEndPoint.X - DragStartPoint.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(DragEndPoint.Y - DragStartPoint.Y) > SystemParameters.MinimumVerticalDragDistance);
            }
            if (!IsDragDowned) return;
            
            if (IsDragAngle)
            {
                VM.AngleLayer(DragStartPoint, DragEndPoint);
            }
            else if (IsDragSize)
            {
                VM.SizeLayer(DragStartPoint, DragEndPoint);
            }
            else
            {
                VM.MoveLayer(DragStartPoint, DragEndPoint);
            }
        }

        #endregion


        #region KeyMove

        double _KeyLeftDelta;
        double _KeyRightDelta;
        double _KeyUpDelta;
        double _KeyDownDelta;

        int _KeyBits;

        void SetKeyBits(int n)
        {
            int oldBits = _KeyBits;
            _KeyBits = _KeyBits | (1 << n);
            if (oldBits == 0 && _KeyBits != 0) App.History.BeginGroup();
        }

        void ResetKeyBits(int n)
        {
            int oldBits = _KeyBits;
            _KeyBits = _KeyBits & ~(1 << n);
            if (oldBits != 0 && _KeyBits == 0) App.History.EndGroup();
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                _KeyLeftDelta = -1.0f;
                SetKeyBits(1);
            }
            else if (e.Key == Key.Right)
            {
                _KeyRightDelta = 1.0f;
                SetKeyBits(2);
            }
            else if (e.Key == Key.Up)
            {
                _KeyUpDelta = -1.0f;
                SetKeyBits(3);
            }
            else if (e.Key == Key.Down)
            {
                _KeyDownDelta = 1.0f;
                SetKeyBits(4);
            }

            VM.MoveLayer(_KeyLeftDelta + _KeyRightDelta, _KeyUpDelta + _KeyDownDelta);
        }

        private void OnPreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                _KeyLeftDelta = 0.0f;
                ResetKeyBits(1);
            }
            else if (e.Key == Key.Right)
            {
                _KeyRightDelta = 0.0f;
                ResetKeyBits(2);
            }
            else if (e.Key == Key.Up)
            {
                _KeyUpDelta = 0.0f;
                ResetKeyBits(3);
            }
            else if (e.Key == Key.Down)
            {
                _KeyDownDelta = 0.0f;
                ResetKeyBits(4);
            }
        }

        private void OnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            int oldBits = _KeyBits;
            _KeyBits = 0;
            if (oldBits != 0 && _KeyBits == 0) App.History.EndGroup();
        }

        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // 終了確認
            if (App.History.IsChanged())
            {
                var result = MessageBox.Show("プロジェクトが保存されていません。\nこのまま終了してよろしいですか？", "終了確認", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result != MessageBoxResult.OK)
                {
                    e.Cancel = true;
                }
            }
        }
    }



    //
    [ValueConversion(typeof(int), typeof(Brush))]
    public class LevelToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, global::System.Globalization.CultureInfo culture)
        {
            if ((int)value == 0)
            {
                return new SolidColorBrush(Color.FromArgb(0xFF, 0xEE, 0xEE, 0xEE));
            }
            else
            {
                return Brushes.White;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, global::System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
